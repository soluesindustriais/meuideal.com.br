<?php
ob_start();
session_start();
require('_app/Config.inc.php');

$login = new Login(1);
$logoff = filter_input(INPUT_GET, 'logoff', FILTER_VALIDATE_BOOLEAN);
$getexe = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);

if (!$login->CheckLogin()):
  unset($_SESSION['userlogin']);
  header('Location: index.php?exe=restrito');
else:
  $userlogin = $_SESSION['userlogin'];

  $empresa = new Read;
  $cidade = new Read;

  $ReadUser = new Read;
  $ReadUser->ExeRead(TB_USERS, 'WHERE user_id = :id', "id={$userlogin['user_id']}");
  if ($ReadUser->getResult()):
    foreach ($ReadUser->getResult() as $user):
      extract($user);
    endforeach;
  endif;

  $empresa->ExeRead(TB_EMP, 'WHERE empresa_id = :useremp', "useremp={$userlogin['user_empresa']}");
  if ($empresa->getResult()):
    foreach ($empresa->getResult() as $emp):
      extract($emp);
      $cidade->ExeRead(TB_CID, 'WHERE cidade_id = :cityid', "cityid={$empresa_cidade}");
      if ($cidade->getResult()):
        foreach ($cidade->getResult() as $city):
          extract($city);
        endforeach;
      endif;
    endforeach;
  endif;
endif;

if ($logoff):
  unset($_SESSION['userlogin']);
  header('Location: index.php?exe=logoff');
endif;
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= RAIZ; ?>/imagens/favicon.png">
    <title><?= SITENAME; ?></title>

    <link href="<?= BASE; ?>/css/style.css" rel="stylesheet">

    <!-- Bootstrap -->        
    <link href="<?= BASE; ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= BASE; ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= BASE; ?>/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= BASE; ?>/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?= BASE; ?>/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">

    <!-- Bootstrap File Input-->
    <link href="<?= BASE; ?>/vendors/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet">

    <!-- JQVMap -->
    <!--<link href="<?= BASE; ?>/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>-->

    <!-- bootstrap-daterangepicker -->
    <link href="<?= BASE; ?>/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Sweet Alert-->
    <link rel="stylesheet" type="text/css" href="<?= BASE; ?>/vendors/bootstrap-sweetalert/lib/sweet-alert.css">

    <!-- Custom Theme Style -->
    <link href="<?= BASE; ?>/build/css/custom.css" rel="stylesheet">

    <!-- Select2 -->
    <link href="<?= BASE; ?>/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?= BASE; ?>/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="<?= BASE; ?>/vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="<?= BASE; ?>/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">

    <!-- Datatables -->
    <link href="<?= BASE; ?>/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?= BASE; ?>/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?= BASE; ?>/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?= BASE; ?>/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?= BASE; ?>/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!--Box modal Fancy -->
    <link rel="stylesheet" href="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!--        <link rel="stylesheet" href="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/helpers/jquery.fancybox-buttons.css" media="screen" />
    <link rel="stylesheet" href="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/helpers/jquery.fancybox-thumbs.css" media="screen" />-->

    <!--FLAG ICONS-->
    <link href="<?= BASE; ?>/vendors/flag-icon-css-master/css/flag-icon.css" rel="stylesheet">

  </head>
  <body class="nav-md">

    <!--<div class="loading"></div>-->

    <div class="container body">
      <div class="main_container">
        <?php
        //ATIVA MENU
        if (isset($getexe)):
          $linkto = explode('/', $getexe);
        else:
          $linkto = array('exe' => 'home');
        endif;

        //QUERY STRING - FRONT CONTROLLER
        require_once('inc/header.inc.php');
        require_once('inc/sidebar.inc.php');

        if (!empty($getexe)):
          $includepatch = __DIR__ . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR . strip_tags(trim($getexe) . '.php');
          /*
           * Box do upload para o tinymce em todas as páginas.
           * O modal só é aberto pela função do tinymce
           */
          include('inc/tinymce-upload-inc.php');
        else:
          $includepatch = __DIR__ . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR . 'profile/profile.php';
        endif;

        if (file_exists($includepatch)):
          require_once($includepatch);
        else:
          include_once(__DIR__ . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR . '404.php');
        endif;
        ?>

        <!-- footer -->
        <footer>
          <div class="pull-right">
            <a href="<?= AGENCY_URL; ?>"><img src="<?= BASE; ?>/images/logo-sig.png" title="<?= AGENCY_NAME; ?>" alt="<?= AGENCY_NAME; ?>" class="img-responsive" style="max-width: 200px;"/></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="<?= BASE; ?>/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Custom JS-->
    <script type="text/javascript" src="<?= BASE; ?>/_cdn/scripts.js"></script>
    <script type="text/javascript" src="<?= BASE; ?>/_cdn/jquery.form.js"></script>
    <script type="text/javascript" src="<?= BASE; ?>/_cdn/jmask.js"></script>
    <script type="text/javascript" src="<?= BASE; ?>/_cdn/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?= BASE; ?>/_cdn/data-tables.js"></script>

    <!-- Bootstrap -->
    <script src="<?= BASE; ?>/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?= BASE; ?>/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?= BASE; ?>/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?= BASE; ?>/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?= BASE; ?>/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?= BASE; ?>/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

    <!-- Bootstrap File Input-->
    <script src="<?= BASE; ?>/vendors/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
    <script src="<?= BASE; ?>/vendors/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="<?= BASE; ?>/_cdn/file-uploads/bootstrap-file-input.js"></script>

    <!-- iCheck -->
    <script src="<?= BASE; ?>/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?= BASE; ?>/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?= BASE; ?>/vendors/Flot/jquery.flot.js"></script>
    <script src="<?= BASE; ?>/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?= BASE; ?>/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?= BASE; ?>/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?= BASE; ?>/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?= BASE; ?>/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?= BASE; ?>/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?= BASE; ?>/vendors/flot.curvedlines/curvedLines.js"></script>

    <!-- Sweet Alert-->
    <script type="text/javascript" src="<?= BASE; ?>/vendors/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>

    <!-- DateJS -->
    <script src="<?= BASE; ?>/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
<!--    <script src="<?= BASE; ?>/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?= BASE; ?>/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?= BASE; ?>/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>-->
    <!-- bootstrap-daterangepicker -->
    <script src="<?= BASE; ?>/vendors/moment/min/moment.min.js"></script>
    <script src="<?= BASE; ?>/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?= BASE; ?>/build/js/custom.min.js"></script>

    <!-- jQuery Tags Input -->
    <script src="<?= BASE; ?>/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="<?= BASE; ?>/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="<?= BASE; ?>/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="<?= BASE; ?>/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="<?= BASE; ?>/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="<?= BASE; ?>/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="<?= BASE; ?>/vendors/starrr/dist/starrr.js"></script>

    <!-- Datatables -->
    <script src="<?= BASE; ?>/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?= BASE; ?>/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?= BASE; ?>/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?= BASE; ?>/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= BASE; ?>/vendors/pdfmake/build/vfs_fonts.js"></script>       

    <!--Box modal Fancy -->
    <script src="<?= BASE; ?>/_cdn/modalbox/app_fancy/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<!--        <script src="<?= BASE; ?>/_cdn/modalboxs/app_fancy/source/helpers/jquery.fancybox-buttons.js"></script>
    <script src="<?= BASE; ?>/_cdn/modalboxs/app_fancy/source/helpers/jquery.fancybox-media.js"></script>
    <script  src="<?= BASE; ?>/_cdn/modalboxs/app_fancy/source/helpers/jquery.fancybox-thumbs.js"></script>-->

  </body>
</html>
<?php
ob_end_flush();
