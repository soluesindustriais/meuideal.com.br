<?php
//View das galerias cadastradas para empresas e produtos
$ReadGalery = new Read;
$ReadGalery->ExeRead(TB_GALLERY, "WHERE gallery_rel = :id AND user_empresa = :emp", "id={$itemId}&emp={$_SESSION['userlogin']['user_empresa']}");
if ($ReadGalery->getResult()):
  foreach ($ReadGalery->getResult() as $gd):
    ?>
    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 j_item" id="<?= $gd['gallery_id']; ?>">           
      <div class="image view view-first" style="border: 1px solid #ccc; border-radius: 2px; margin: 10px 0px;">
        <?= Check::Image('../doutor/uploads/' . $gd['gallery_file'], $itemtitle, 'img-responsive', 300, 300); ?>
        <div class="mask no-caption">
          <div class="tools tools-bottom btn-group-xs">
            <button type="button" class="btn btn-primary j_modalBox" href="<?= BASE . '/uploads/' . $gd['gallery_file']; ?>" title="<?= $itemtitle; ?>"><i class="fa fa-eye"></i></button>
            <button type="button" class="btn btn-danger j_remove" rel="<?= $gd['gallery_id']; ?>" action="RemoveImagem"><i class="fa fa-times"></i></button>
          </div>
        </div>
      </div>
    </div>
    <?php
  endforeach;
endif;