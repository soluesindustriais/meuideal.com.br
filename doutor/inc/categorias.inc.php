<select name="cat_parent" class="form-control col-md-7 col-xs-12" required="required">
    <?php
    $ReadRecursos = new Read;
    $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent IS NULL ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}");
    ?>
    <option value="" selected="selected" class="text-info text-bold" style="padding: 10px;">-- Selecione --</option>
    <?php
    if ($ReadRecursos->getResult()):
        foreach ($ReadRecursos->getResult() as $key):
            ?>
            <option value="<?= $key['cat_id'] ?>" <?php
            if (isset($post['cat_parent']) && $post['cat_parent'] == $key['cat_id']): echo 'selected="selected"';
            elseif (isset($dados) && $dados['cat_parent'] == $key['cat_id']): echo 'selected="selected"';
            endif;
            ?> style="padding:5px;">» <?= $key['cat_title']; ?></option>
                    <?php
                    $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$key['cat_id']}");
                    foreach ($ReadRecursos->getResult() as $key):
                        ?>
                <option value="<?= $key['cat_id']; ?>" <?php
                if (isset($post['cat_parent']) && $post['cat_parent'] == $key['cat_id']): echo 'selected="selected"';
                elseif (isset($dados) && $dados['cat_parent'] == $key['cat_id']): echo 'selected="selected"';
                endif;
                ?> style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $key['cat_title']; ?></option>
                        <?php
                        $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$key['cat_id']}");
                        if ($ReadRecursos->getResult()):
                            foreach ($ReadRecursos->getResult() as $keys):
                                ?>
                        <option value="<?= $keys['cat_id']; ?>" <?php
                        if (isset($post['cat_parent']) && $post['cat_parent'] == $keys['cat_id']): echo 'selected="selected"';
                        elseif ($keys['cat_id'] == $dados['cat_parent']): echo 'selected="selected"';
                        endif;
                        ?> style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow;<?= $keys['cat_title']; ?></option>
                        <?php
                    endforeach;
                endif;
            endforeach;
        endforeach;
    endif;
    ?>
</select>