<select name="sl_url" class="form-control col-md-7 col-xs-12">
  <option value="" selected="selected" class="text-info text-bold" style="padding: 10px;">-- Selecione --</option>
  <?php
  $Read = new Read;
  $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent IS NULL ORDER BY cat_order ASC", "emp=" . EMPRESA_CLIENTE);
  if ($Read->getResult()):
    foreach ($Read->getResult() as $sessao):
      ?>
      <!--NÍVEL DA SESSÃO-->
      <option value="<?= trim(Check::CatByParent($sessao['cat_id'], EMPRESA_CLIENTE), '/'); ?>" style="padding:5px;" 
      <?php
      if (trim(Check::CatByParent($sessao['cat_id'], EMPRESA_CLIENTE), '/') == $sl_url || isset($post['sl_url'])):
        echo 'selected="selected"';
      endif;
      ?>>» <?= $sessao['cat_title'] ?></option>
              <?php
              $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent = :id ORDER BY cat_date ASC", "emp=" . EMPRESA_CLIENTE . "&id={$sessao['cat_id']}");
              if ($Read->getResult()):
                foreach ($Read->getResult() as $categ):
                  ?>
          <option value="<?= trim(Check::CatByParent($categ['cat_id'], EMPRESA_CLIENTE), '/'); ?>" style="color: #bbb; padding: 4px;"
          <?php
          if (trim(Check::CatByParent($categ['cat_id'], EMPRESA_CLIENTE), '/') == $sl_url || isset($post['sl_url'])):
            echo 'selected="selected"';
          endif;
          ?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $categ['cat_title'] ?></option> 
                  <?php
                  $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent = :id ORDER BY cat_date ASC", "emp=" . EMPRESA_CLIENTE . "&id={$categ['cat_id']}");
                  if (!$Read->getResult()):
                    $itemCat = $categ['cat_id'];
                    include("inc/url-itens-inc.php");
                  else:
                    foreach ($Read->getResult() as $subcateg):
                      ?>
              <!--//NÍVEL DA SUBCATEGORIA--> 
              <option value="<?= trim(Check::CatByParent($subcateg['cat_id'], EMPRESA_CLIENTE), '/'); ?>" style="color: #bbb; padding: 4px;"
                      <?php
                      if (trim(Check::CatByParent($subcateg['cat_id'], EMPRESA_CLIENTE), '/') == $sl_url || isset($post['sl_url'])):
                        echo 'selected="selected"';
                      endif;
                      ?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $subcateg['cat_title'] ?></option>
              <!--//NÍVEL FINAL DO ITEM-->
              <?php
              $itemCat = $subcateg['cat_id'];
              include("inc/url-itens-inc.php");
            endforeach;
            $itemCat = $subcateg['cat_parent'];
            include("inc/url-itens-inc.php");
          endif;
        endforeach;
        $itemCat = $categ['cat_parent'];
        include("inc/url-itens-inc.php");
      endif;

    endforeach;
  endif;
  ?>
</select>