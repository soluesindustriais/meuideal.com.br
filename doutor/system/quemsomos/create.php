<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <form data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
        <div class="page-title">

            <div class="title col-md-12 col-sm-6 col-xs-12">
                <h3><i class="fa fa-bank"></i> Cadastro de páginas da empresa</h3>
            </div>
            <div class="clearfix"></div>
            <br/>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="pull-right">
                        <button type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=quemsomos/index'"><i class="fa fa-home"></i></button>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['Cadastra'])):
                        unset($post['Cadastra']);
                        $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
                        $post['quem_cover'] = ( $_FILES['quem_cover']['tmp_name'] ? $_FILES['quem_cover'] : null );
                        $post['gallery_file'] = ( $_FILES['gallery_file']['tmp_name'] ? $_FILES['gallery_file'] : null );

                        $cadastra = new QuemSomos();
                        $cadastra->ExeCreate($post);

                        if (!$cadastra->getResult()):
                            $erro = $cadastra->getError();
                            WSErro($erro[0], $erro[1], null, $erro[2]);
                        else:
                            $_SESSION['Error'] = $cadastra->getError();
                            header('Location: painel.php?exe=quemsomos/create&get=true');
                        endif;


                    endif;

                    if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                        unset($_SESSION['Error']);
                    endif;
                    ?>                    
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="x_title">
                                <h2> Abaixo você você pode cadastrar as páginas para expor as informações sobre sua empresa.</h2>                           
                                <div class="clearfix"></div>                            
                            </div>

                            <div class="clearfix"></div>
                            <br>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quem_status">Publicar agora?</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="radio">
                                    <label>
                                        <input name="quem_status" id="optionsRadios1" type="radio" value="2" <?php
                                        if (isset($post['quem_status']) && $post['quem_status'] == 2): echo 'checked="true"';
                                        endif;
                                        ?>> Sim
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input name="quem_status" id="optionsRadios2" type="radio" value="1" <?php
                                        if (isset($post['quem_status']) && $post['quem_status'] == 1): echo 'checked="true"';
                                        endif;
                                        ?>> Não
                                    </label>
                                </div>                 
                            </div>                                 
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_parent">Sessão ou categoria do item <span class="required">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <!--inclusão das seleções de categorias-->
                                <?php include("inc/categorias.inc.php"); ?>
                                <!-- /inclusão das seleções de categorias-->
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quem_cover">Capa <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="file" id="app_cover" name="quem_cover" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quem_title">Nome <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="quem_title" name="quem_title" required="required" class="form-control col-md-7 col-xs-12" value="<?php
                                if (isset($post['quem_title'])): echo $post['quem_title'];
                                endif;
                                ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quem_description">Descrição, até 160 caractéres <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea name="quem_description" 
                                          required="required" 
                                          class="form-control col-md-7 col-xs-12 j_contChars" 
                                          data-parsley-trigger="keyup" 
                                          data-parsley-minlength="140" 
                                          data-parsley-maxlength="160" 
                                          data-parsley-minlength-message="Você deve digitar no mínimo 140 caracteres!" 
                                          data-parsley-validation-threshold="139"><?php
                                              if (isset($post['quem_description'])): echo $post['quem_description'];
                                              endif;
                                              ?></textarea>
                                <div class="j_cont"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quem_content">Conteúdo da página <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea name="quem_content" id="quem_content" class="j_word form-control col-md-7 col-xs-12" rows="10"><?php
                                    if (isset($post['quem_content'])): echo $post['quem_content'];
                                    endif;
                                    ?></textarea>                                
                            </div>
                        </div>
                        <div class="x_content">
                            <div class="x_title">
                                <h2>Galeria de imagens do item</h2>                           
                                <div class="clearfix"></div>                            
                            </div>

                            <div class="clearfix"></div>
                            <br>
                        </div>                        

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input id="app_gallery" name="gallery_file[]" type="file" multiple="">
                                <p class="help-block">Selecione até 15 imagens para adicionar ao seu item.</p>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <button type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=quemsomos/index'"><i class="fa fa-home"></i></button>
        </div>
        <div class="clearfix"></div>
        <br>
    </form>
    <div class="clearfix"></div>
</div>
