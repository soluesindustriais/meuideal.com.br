<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 3;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <form id="demo-form2" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" data-parsley-validate>
        <div class="page-title">
            <div class="title col-md-12 col-sm-6 col-xs-12">
                <h3><i class="fa fa-user"></i> Atualizar de usuários</h3> 
            </div>
            <div class="clearfix"></div>                   
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="pull-right">
                        <button type="submit" name="UpdateUser" class="btn btn-primary"><i class="fa fa-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=users/index'"><i class="fa fa-home"></i></button>
                    </div>
                    <div class="clearfix"></div> 
                    <br/>
                    <?php
                    $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['UpdateUser'])):
                        unset($post['UpdateUser']);

                        $update = new AdminUsers($Data);
                        $update->ExeUpdate($get, $post);

                        if (!$update->getResult()):
                            $erro = $update->getError();
                            WSErro($erro[0], $erro[1], null, $erro[2]);
                        else:
                            $_SESSION['Error'] = $update->getError();
                            header('Location: painel.php?exe=users/index&get=true');
                        endif;
                    endif;

                    $ReadUser = new Read;
                    $ReadUser->ExeRead(TB_USERS, "WHERE user_id = :id AND user_empresa = :emp", "id={$get}&emp={$_SESSION['userlogin']['user_empresa']}");
                    if (!$ReadUser->getResult()):
                        WSErro("Desculpe, usuário não encontrado.", WS_ERROR, null, "Doutores da Web");
                    else:
                        foreach ($ReadUser->getResult() as $upuser):
                            extract($upuser);
                            ?> 
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">                            
                                            <h2>Dados pessoais</h2>    
                                            <div class="clearfix"></div>
                                        </div>  
                                        <br/>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtFirstName">Nome</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="txtFirstName" type="text" name="user_name" value="<?php
                                                if (isset($post['user_name'])): echo $post['user_name'];
                                                else: echo $user_name;
                                                endif;
                                                ?>" placeholder="Digite o nome" required="required" data-rule-rangelength="[1,30]" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtLastName">Sobrenome</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="txtLastName" type="text" name="user_lastname" value="<?php
                                                if (isset($post['user_lastname'])): echo $post['user_lastname'];
                                                else: echo $user_lastname;
                                                endif;
                                                ?>" placeholder="Digite o sobrenome" required="required" data-rule-rangelength="[1,50]" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtCustomerName">Cargo</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="txtCustomerName" type="text" name="user_cargo" value="<?php
                                                if (isset($post['user_cargo'])): echo $post['user_cargo'];
                                                else: echo $user_cargo;
                                                endif;
                                                ?>" placeholder="Digite o cargo" required="required" data-rule-rangelength="[1,50]" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtDatePicker">Data de nascimento</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <div data-format="DD/MM/YYYY" class="input-group">
                                                    <input id="j_nasc" type="text" data-mask="99/99/9999" required="required" name="user_nasc" value='<?php
                                                    if (isset($post['user_nasc'])): echo $post['user_nasc'];
                                                    else: echo date("d/m/Y", strtotime($user_nasc));
                                                    endif;
                                                    ?>' placeholder="00/00/0000" required="required" data-mask="99/99/9999" data-rule-minlength="8" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtPhoneNumber">Telefone <span class="text-info">(opcional)</span></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="txtPhoneNumber" data-mask="(99) 9 99999999" type="text" name="user_fone" value="<?php
                                                if (isset($post['user_fone'])): echo $post['user_fone'];
                                                else: echo $user_fone;
                                                endif;
                                                ?>" placeholder="(99) 9 9999-9999" data-rule-phonenumber="true" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtDigits">Ramal <span class="text-info">(opcional)</span></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="txtDigits" type="text" name="user_ramal" value="<?php
                                                if (isset($post['user_ramal'])): echo $post['user_ramal'];
                                                else: echo $user_ramal;
                                                endif;
                                                ?>" placeholder="Digite o ramal"  data-rule-digits="true" data-mask="99999" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_genero">Gênero</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <div class="radio">
                                                    <label>
                                                        <input name="user_genero" id="optionsRadios1" required="required" type="radio" value="Masculino" <?php
                                                        if ($user_genero == 'Masculino'): echo 'checked="true"';
                                                        endif;
                                                        ?>> Masculino
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input name="user_genero" id="optionsRadios2" type="radio" value="Feminino" <?php
                                                        if ($user_genero == 'Feminino'): echo 'checked="true"';
                                                        endif;
                                                        ?>>Feminino
                                                    </label>
                                                </div>                 
                                            </div>                                 
                                        </div> 
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">                            
                                            <h2>Dados de acesso</h2>    
                                            <div class="clearfix"></div>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_level">Nível de acesso</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select id="user_level" name="user_level" required="required" class="form-control">
                                                    <option value="">-- Selecione o nível do usuário --</option> 
                                                    <?php foreach (GetNiveisUser() as $key => $value): ?>
                                                        <option value="<?= $key; ?>" <?php
                                                        if (isset($post['user_level']) && $post['user_level'] == $key): echo 'selected="selected"';
                                                        elseif ($user_level == $key): echo 'selected="selected"';
                                                        endif;
                                                        ?>><?= $value; ?></option>
                                                            <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtEmail">E-mail</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="txtEmail" type="text" name="user_email" value="<?php
                                                if (isset($post['user_email'])): echo $post['user_email'];
                                                else: echo $user_email;
                                                endif;
                                                ?>" placeholder="Digite o e-mail" required="required" data-rule-rangelength="[10,70]" data-rule-email="true" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtPassword">Senha <span class="text-info">(gerada automaticamente)</span></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="txtPassword" type="password" name="user_password" placeholder="Senha"  data-rule-rangelength="[6,30]" class="form-control" autocomplete="false">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="txtConfirmPassword">Confirme a senha</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="txtConfirmPassword" type="password" name="txtConfirmPassword" placeholder="Confirme a senha" data-rule-required="false" data-rule-equalto="#txtPassword" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                    

                            <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <button type="submit" name="UpdateUser" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=users/index'"><i class="fa fa-home"></i></button>
        </div>
        <div class="clearfix"></div> 
        <br/>
    </form>
</div>