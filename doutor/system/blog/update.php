<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 2;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
    <div class="page-title">
      <div class="title col-md-12 col-sm-6 col-xs-12">
        <h3><i class="fa fa-quote-left"></i> Atualização de postagens do Blog</h3>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="pull-right">
            <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=blog/index'"><i class="fa fa-home"></i></button>
          </div>
          <div class="clearfix"></div>
          <br>
          <?php
          $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['Update'])):
            unset($post['Update']);
            $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
            $post['blog_cover'] = ( $_FILES['blog_cover']['tmp_name'] ? $_FILES['blog_cover'] : null );
            $post['gallery_file'] = ( $_FILES['gallery_file']['tmp_name'] ? $_FILES['gallery_file'] : null );
            $post['blog_id'] = $get;

            $update = new Blog();
            $update->ExeUpdate($post);

            if (!$update->getResult()):
              $erro = $update->getError();
              WSErro($erro[0], $erro[1], null, $erro[2]);
            else:
              $_SESSION['Error'] = $update->getError();
              header('Location: painel.php?exe=blog/index&get=true');
            endif;
          endif;

          $ReadRecursos = new Read;
          $ReadRecursos->ExeRead(TB_BLOG, "WHERE user_empresa = :emp AND blog_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$get}");
          if ($ReadRecursos->getResult()):
            foreach ($ReadRecursos->getResult() as $dados):
              extract($dados);
              ?>                   
              <div class="x_panel">
                <div class="x_content">
                  <div class="x_title">
                    <h2>Edite sua postagem do blog.</h2>                           
                    <div class="clearfix"></div>                            
                  </div>

                  <div class="clearfix"></div>
                  <br>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_genero">Publicar agora?</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="radio">
                        <label>
                          <input name="blog_status" id="optionsRadios1" required="required" type="radio" value="2" <?php
                          if ($post['blog_status'] == 2): echo 'checked="true"';
                          elseif ($blog_status == 2): echo 'checked="true"';
                          endif;
                          ?>> Sim
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input name="blog_status" id="optionsRadios2" type="radio" value="1" <?php
                          if ($post['blog_status'] == 1): echo 'checked="true"';
                          elseif ($blog_status == 1): echo 'checked="true"';
                          endif;
                          ?>> Não
                        </label>
                      </div>                 
                    </div>                                 
                  </div> 

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_parent">Sessão ou categoria do item <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <!--inclusão das seleções de categorias-->
                      <?php include("inc/categorias.inc.php"); ?>
                      <!-- /inclusão das seleções de categorias-->
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="blog_cover">Capa, <span class="text-danger">ao reenviar, o arquivo antigo será substituído</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <?= Check::Image("../doutor/uploads/" . $blog_cover, $blog_title, 'img-responsive', 100); ?>
                      <div class="clearfix"></div>
                      <br/>
                      <input type="file" id="app_cover" name="blog_cover">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="blog_title">Nome <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="text" id="blog_title" name="blog_title" required="required" class="form-control col-md-7 col-xs-12" value="<?php
                      if (isset($post['blog_title'])): echo $post['blog_title'];
                      else: echo $blog_title;
                      endif;
                      ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="blog_description">Descrição, até 160 caractéres <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea name="blog_description" 
                                required="required" 
                                class="form-control col-md-7 col-xs-12 j_contChars" 
                                data-parsley-trigger="keyup" 
                                data-parsley-minlength="140" 
                                data-parsley-maxlength="160" 
                                data-parsley-minlength-message="Você deve digitar no mínimo 140 caracteres!" 
                                data-parsley-validation-threshold="139"><?php
                                  if (isset($post['blog_description'])): echo $post['blog_description'];
                                  else: echo $blog_description;
                                  endif;
                                  ?></textarea>
                      <div class="j_cont"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="blog_content">Conteúdo da página <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea name="blog_content" id="blog_content" class="j_word form-control col-md-7 col-xs-12" rows="10"><?php
                        if (isset($post['blog_content'])): echo $post['blog_content'];
                        else: echo $blog_content;
                        endif;
                        ?></textarea>                                
                    </div>
                  </div>

                  <div class="x_content">
                    <div class="x_title">
                      <h2>Galeria de imagens do item</h2>                           
                      <div class="clearfix"></div>                            
                    </div>

                    <div class="clearfix"></div>
                    <br>
                  </div>                        

                  <!--Galeia global view-->
                  <?php
                  $itemId = (int) $blog_id;
                  $itemtitle = (string) $blog_title;
                  include("inc/gallery-inc.php");
                  ?>
                  <!-- /Galeia global view-->

                  <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <input id="app_gallery" name="gallery_file[]" type="file" multiple="">
                      <p class="help-block">Selecione até 6 imagens para adicionar ao seu item.</p>
                    </div>
                  </div> 


                </div>
              </div>
              <?php
            endforeach;
          endif;
          ?>
        </div>
      </div>
    </div>
    <div class="pull-right">
      <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=blog/index'"><i class="fa fa-home"></i></button>
    </div>
    <div class="clearfix"></div>
    <br>
  </form>
  <div class="clearfix"></div>
</div>
