<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 5;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <div class="page-title">
        <div class="title col-md-12 col-sm-6 col-xs-12">
            <h3><i class="fa fa-th-list"></i> Histórico de operações do sistema</h3>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12"> 
                <div class="x_panel">
                    <div class="x_content">   
                        <div class="x_title">
                            <h2>Utilize o campo de pesquisa para filtrar sua busca.<small>Você também pode acessar, editar e alterar status de acesso das empresas.</small></h2>                           
                            <div class="clearfix"></div>                            
                        </div>
                        <?php
                        $Read = new Read;
                        $Read->ExeRead(TB_HISTORICO, "WHERE h_empresa = :emp ORDER BY h_data DESC", "emp=" . EMPRESA_MASTER . "");
                        if (!$Read->getResult()):
                            WSErro("Desculpe, não existem informações cadastradas no histórico.", WS_INFOR, null, "Doutores da Web");
                        else:
                            ?>                             
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped dtr-inline dataTable jambo_table">                            
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Cargo</th>
                                            <th>Acesso</th>
                                            <th>Item</th>
                                            <th>Ação</th>
                                            <th>Data/Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($Read->getResult() as $key):
                                            extract($key);
                                            ?>
                                            <tr>
                                                <td><?= Check::GetUserName($h_user); ?></td>
                                                <td><?= $h_cargo ?></td>
                                                <td><?= GetNiveis($h_nivel) ?></td>
                                                <td><?= $h_item ?></td>
                                                <td><?= $h_acao ?></td>
                                                <td><?= date("d/m/Y H:i:s", strtotime($h_data)); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif; ?>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /page content -->