<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 2;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
    <div class="page-title">
      <div class="title col-md-12 col-sm-6 col-xs-12">
        <h3><i class="fa fa-picture-o"></i>  Atualização do case</h3>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="pull-right">
            <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=cases/index'"><i class="fa fa-home"></i></button>
          </div>
          <div class="clearfix"></div>
          <br>
          <?php
          $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['Update'])):
            unset($post['Update']);
            $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
            $post['case_cover'] = ( $_FILES['case_cover']['tmp_name'] ? $_FILES['case_cover'] : null );
            $post['gallery_file'] = ( $_FILES['gallery_file']['tmp_name'] ? $_FILES['gallery_file'] : null );
            $post['case_id'] = $get;

            $update = new Cases();
            $update->ExeUpdate($post);

            if (!$update->getResult()):
              $erro = $update->getError();
              WSErro($erro[0], $erro[1], null, $erro[2]);
            else:
              $_SESSION['Error'] = $update->getError();
              header('Location: painel.php?exe=cases/index&get=true');
            endif;
          endif;

          $ReadRecursos = new Read;
          $ReadRecursos->ExeRead(TB_CASE, "WHERE user_empresa = :emp AND case_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$get}");
          if ($ReadRecursos->getResult()):
            foreach ($ReadRecursos->getResult() as $dados):
              extract($dados);
              ?>                   
              <div class="x_panel">
                <div class="x_content">
                  <div class="x_title">
                    <h2>Edite as informações do case.</h2>                           
                    <div class="clearfix"></div>                            
                  </div>

                  <div class="clearfix"></div>
                  <br>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_genero">Publicar agora?</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="radio">
                        <label>
                          <input name="case_status" id="optionsRadios1" required="required" type="radio" value="2" <?php
                          if ($post['case_status'] == 2): echo 'checked="true"';
                          elseif ($case_status == 2): echo 'checked="true"';
                          endif;
                          ?>> Sim
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input name="case_status" id="optionsRadios2" type="radio" value="1" <?php
                          if ($post['case_status'] == 1): echo 'checked="true"';
                          elseif ($case_status == 1): echo 'checked="true"';
                          endif;
                          ?>> Não
                        </label>
                      </div>                 
                    </div>                                 
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_parent">Sessão ou categoria do item <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <!--inclusão das seleções de categorias-->
                      <?php include("inc/categorias.inc.php"); ?>
                      <!-- /inclusão das seleções de categorias-->
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="case_cover">Capa, <span class="text-danger">ao reenviar, o arquivo antigo será substituído</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <?= Check::Image("../doutor/uploads/" . $case_cover, $case_title, 'img-responsive', 200); ?>
                      <div class="clearfix"></div>
                      <br/>
                      <input type="file" id="app_cover" name="case_cover">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="case_title">Nome <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="text" id="case_title" name="case_title" required="required" class="form-control col-md-7 col-xs-12" value="<?php
                      if (isset($post['case_title'])): echo $post['case_title'];
                      else: echo $case_title;
                      endif;
                      ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="case_description">Descrição, até 160 caractéres <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea name="case_description" 
                                required="required" 
                                class="form-control col-md-7 col-xs-12 j_contChars" 
                                data-parsley-trigger="keyup" 
                                data-parsley-minlength="140" 
                                data-parsley-maxlength="160" 
                                data-parsley-minlength-message="Você deve digitar no mínimo 140 caracteres!" 
                                data-parsley-validation-threshold="139"><?php
                                  if (isset($post['case_description'])): echo $post['case_description'];
                                  else: echo $case_description;
                                  endif;
                                  ?></textarea>
                      <div class="j_cont"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="case_content">Conteúdo da página <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea name="case_content" id="case_content" class="j_word form-control col-md-7 col-xs-12" rows="10"><?php
                        if (isset($post['case_content'])): echo $post['case_content'];
                        else: echo $case_content;
                        endif;
                        ?></textarea>                                
                    </div>
                  </div>

                  <div class="x_content">
                    <div class="x_title">
                      <h2>Galeria de imagens do item</h2>                           
                      <div class="clearfix"></div>                            
                    </div>

                    <div class="clearfix"></div>
                    <br>
                  </div>                        

                  <!--Galeia global view-->
                  <?php
                  $itemId = (int) $case_id;
                  $itemtitle = (string) $case_title;
                  include("inc/gallery-inc.php");
                  ?>
                  <!-- /Galeia global view-->

                  <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <input id="app_gallery_full" name="gallery_file[]" type="file" multiple="">
                      <p class="help-block">Selecione até 20 imagens para adicionar ao seu item.</p>
                    </div>
                  </div> 

                </div>
              </div>
              <?php
            endforeach;
          endif;
          ?>
        </div>
      </div>
    </div>
    <div class="pull-right">
      <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=cases/index'"><i class="fa fa-home"></i></button>
    </div>
    <div class="clearfix"></div>
    <br>
  </form>
  <div class="clearfix"></div>
</div>
