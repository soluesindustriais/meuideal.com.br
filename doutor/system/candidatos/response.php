<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <div class="page-title">
        <div class="title_left">
            <h3>Titulo</h3>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12"> 
                <div class="x_panel">
                        <div class="x_title">
                            <h2>Utilize o campo de pesquisa para filtrar sua busca.<small>Você também pode acessar, editar e alterar status de acesso das empresas.</small></h2>                           
                            <div class="clearfix"></div>                            
                        </div>
                        <br/>  
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            Visualize e exporte seus...
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
