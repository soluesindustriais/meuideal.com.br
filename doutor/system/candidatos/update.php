<div class="page-container">
    <div class="page-header clearfix">
        <div class="row">
            <?php
            $lv = 3;
            if (!APP_USERS || empty($userlogin) || $user_level < $lv):
                die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
            endif;
            ?>
            <div class="col-sm-6">
                <h4 class="mt-0 mb-5">Edição de condicionantes</h4>
                <ol class="breadcrumb mb-0">
                    <li><a href="painel.php">Doutores da Web</a></li>   
                    <li><a href="javascript:;">Condicionantes</a></li> 
                    <li class="active">Editar</li>
                </ol>
            </div>
        </div>
    </div>

    <form id="form-vertical" method="post" novalidate="novalidate" enctype="multipart/form-data">
        <div class="page-content container-fluid">

            <div class="widget">
                <div class="widget-heading clearfix">
                    <h3 class="widget-title pull-left">Edição de condicionantes</h3>
                    <div class="pull-right">
                        <button type="submit" name="UpdateCond" class="btn btn-primary"><i class="ti-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=condicionante/index'"><i class="ti-share-alt"></i></button>
                    </div>
                </div>
                <div class="widget-body">
                    <?php
                    $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['UpdateCond'])):
                        $post['cond_file'] = ( $_FILES['cond_file']['tmp_name'] ? $_FILES['cond_file'] : null );
                        $post['cond_id'] = $get;
                        $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
                        unset($post['UpdateCond']);

                        $update = new AdminCondicionantes;
                        $update->ExeUpdate($post);

                        if (!$update->getResult()):
                            WSErro($update->getError()[0], $update->getError()[1], null, $update->getError()[2]);
                        else:
                            WSErro($update->getError()[0], $update->getError()[1], null, $update->getError()[2]);
                        endif;
                    endif;

                    $Read = new Read;
                    $Read->ExeRead(TB_CONDICIONANTES, "WHERE cond_id = :id AND user_empresa = :emp", "id={$get}&emp={$_SESSION['userlogin']['user_empresa']}");
                    if (!$Read->getResult()):
                        WSErro("Não foi possível encontrar a condicionante.", WS_ERROR, null, "Doutores da Web");
                    else:
                        extract($Read->getResult()[0]);
                        ?>  
                        <div class="form-group">
                            <label for="fulImage">Arquivo PDF <span class="text-danger">(Ao enviar um novo arquivo, o antigo será substituído)</span></label>
                            <input id="fulImage" type="file" name="cond_file" data-buttontext="Procurar arquivo" data-buttonname="btn btn-danger" data-iconname="ti-file" data-rule-required="false"  class="filestyle">                                          
                        </div>
                        <div class="form-group">
                            <label for="Nome">Nome da condicionante</label>
                            <input id="Nome" type="text" name="cond_name" value="<?php
                            if (isset($post['cond_name'])): echo $post['cond_name'];
                            else:
                                echo $cond_name;
                            endif;
                            ?>" placeholder="Digite um nome para a condicionante" data-rule-required="true" data-rule-rangelength="[1,50]" class="form-control">
                        </div> 
                        <div class="form-group">
                            <label for="Tipo">Tipo de condicionante</label>
                            <select id="Tipo" name="cond_categ" class="form-control j_categ" data-rule-required="true">
                                <option value="" selected="true">-- Selecione --</option>
                                <?php foreach (GetCategCondicionate() as $key => $value): ?>
                                    <option value="<?= $key; ?>" <?php
                                    if (isset($post['cond_categ']) && $post['cond_categ'] == $key): echo 'selected';
                                    elseif ($key == $cond_id): echo 'selected';
                                    endif;
                                    ?>><?= $value; ?></option> 
                                 <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Link">Relacionar condicionante</label>
                            <select id="Link" name="cond_link" class="form-control j_link" data-rule-required="true">                                
                                <?php if (!isset($post['cond_link']) && !isset($cond_link)): ?>
                                    <option value="" selected disabled>-- Antes selecione o tipo --</option>
                                    <?php
                                else:
                                    $cond = (int) ( isset($cond_categ) ? $cond_categ : $post['cond_categ'] );
                                    if ($cond == 1):
                                        $CondCat = new Read;
                                        $CondCat->ExeRead(TB_OUTORGAS, "WHERE user_empresa = :id", "id={$_SESSION['userlogin']['user_empresa']}");
                                        if ($CondCat->getRowCount()):
                                            foreach ($CondCat->getResult() as $item):
                                                extract($item);
                                                echo "<option value=\"{$out_id}\" ";
                                                if (isset($post['cond_link']) && $post['cond_link'] == $out_id): echo "selected";
                                                elseif ($out_id == $cond_link): echo "selected";
                                                endif;
                                                echo "> " . Check::GetRelationsCond($cond, $out_id) . " </option>";
                                            endforeach;
                                        endif;
                                    elseif ($cond == 2):
                                        $CondCat = new Read;
                                        $CondCat->ExeRead(TB_REGULARIZACAO, "WHERE user_empresa = :id", "id={$_SESSION['userlogin']['user_empresa']}");
                                        if ($CondCat->getRowCount()):
                                            foreach ($CondCat->getResult() as $item):
                                                extract($item);
                                                echo "<option value=\"{$reg_id}\" ";
                                                if (isset($post['cond_link']) && $post['cond_link'] == $reg_id): echo "selected";
                                                elseif ($reg_id == $cond_link): echo "selected";
                                                endif;
                                                echo "> " . Check::GetRelationsCond($cond, $reg_id) . " </option>";
                                            endforeach;
                                        endif;
                                    endif;
                                endif;
                                ?>                                
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="Observacao">Observações</label>
                            <textarea name="cond_obs" id="Observacao" rows="3" class="form-control" placeholder="Digite qualquer observação que deva ser adicionada à condicionante"><?php
                                if (isset($post['cond_obs'])): echo $post['cond_obs'];
                                else:
                                    echo $cond_obs;
                                endif;
                                ?></textarea>
                        </div>
<?php endif; ?>
                </div>
            </div>

        </div>
    </form>
</div>