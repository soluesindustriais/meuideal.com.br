<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 3;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form id="demo-form2" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" data-parsley-validate>
    <div class="page-title">
      <div class="title col-md-12 col-sm-12 col-xs-12">
        <h3><i class="fa fa-language"></i> Cadastro de Idiomas</h3>
      </div>

      <div class="clearfix"></div>
      <div class="pull-right">
        <button type="submit" name="CadastraIdioma" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=idiomas/index'"><i class="fa fa-home"></i></button>
      </div>
      <div class="clearfix"></div> 

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
          <div class="x_panel">          

            <div class="x_title">   
              <h2>Cadastre os idiomas que irão aparecer em seu site.<small>Você pode definir uma bandeira para cada idioma.</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
              </ul>
              <div class="clearfix"></div>
              <br class="clearfix" />
            </div>

            <?php
            $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
            $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            if (isset($post) && isset($post['CadastraIdioma'])):

              unset($post['CadastraIdioma']);              
              $cadastra = new Idiomas;
              $cadastra->ExeCreate($post);              
              
              if (!$cadastra->getResult()):
                $erro = $cadastra->getError();
                WSErro($erro[0], $erro[1], null, $erro[2]);
              else:
                $_SESSION['Error'] = $cadastra->getError();
                header('Location: painel.php?exe=idiomas/create&get=true');
              endif;
            endif;

            if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
              //COLOCAR ALERTA PERSONALIZADOS
              WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
              unset($_SESSION['Error']);
            endif;
            ?>                   


            <div class="x_content">  

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_idioma">Bandeira do idioma <span class="required">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="input-group">
                    <select id="user_level" name="empresa_idioma" required="required" class="form-control">
                      <option value="" class="j_flags" data-flag="">-- Selecione o país --</option> 
                      <?php
                      $ReadIdioma = new Read;
                      $ReadIdioma->ExeRead(TB_PAISES);
                      if ($ReadIdioma->getResult()):
                        foreach ($ReadIdioma->getResult() as $key => $value):
                          ?>
                          <option value="<?= $value['country_id']; ?>" <?php
                          if (isset($post['empresa_idioma']) && $post['empresa_idioma'] == $value['country_id']): echo 'selected="selected"';
                          endif;
                          ?> class="j_flags" data-flag="<?= $value['country_sigla']; ?>"><?= $value['country_name']; ?></option>
                                  <?php
                                endforeach;
                              endif;
                              ?>
                    </select>
                    <span class="input-group-addon j_bandeira">
                      <i class="fa fa-flag" aria-hidden="true"></i>
                    </span>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_idioma_name">Nome do idioma <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" id="empresa_idioma_name" name="empresa_idioma_name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Digite o nome do idioma: Português Brasil" value="<?php
                  if (isset($post['empresa_idioma_name'])): echo $post['empresa_idioma_name'];
                  endif;
                  ?>">
                </div>
              </div>  
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_idioma_sg">Sigla do idioma <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" id="empresa_idioma_sg" name="empresa_idioma_sg" required="required" class="form-control col-md-7 col-xs-12" placeholder="Digite o a sigla do idioma: en, br, pt, ar, es" value="<?php
                  if (isset($post['empresa_idioma_sg'])): echo $post['empresa_idioma_sg'];
                  endif;
                  ?>" data-parsley-trigger="keyup" data-parsley-minlength="2" data-parsley-maxlength="3" data-parsley-maxlength-message="Você deve digitar no máximo 3 caracteres!" data-parsley-validation-threshold="3" maxlength="3">
                </div>
              </div>   
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_idioma_style">Estilo CSS do idioma</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea id="empresa_idioma_style" name="empresa_idioma_style" rows="10" class="form-control col-md-7 col-xs-12" placeholder="<style> *{ //Seu código aqui } </style>" value="<?php
                  if (isset($post['empresa_idioma_style'])): echo $post['empresa_idioma_style'];
                  endif;
                  ?>"></textarea>
                </div>
              </div>                

            </div>

          </div>
        </div>
      </div>    

      <div class="pull-right">
        <button type="submit" name="CadastraIdioma" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=idiomas/index'"><i class="fa fa-home"></i></button>
      </div> 
      <div class="clearfix"></div> 
      <br/>
    </div>
  </form>
</div>

