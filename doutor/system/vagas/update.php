<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 2;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form data-parsley-validate class="form-horizontal form-label-left" method="post">
    <div class="page-title">
      <div class="title col-md-12 col-sm-6 col-xs-12">
        <h3><i class="fa fa-building-o"></i> Editar Área ou Vaga</h3>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
          <div class="pull-right">
            <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=vagas/index'"><i class="fa fa-home"></i></button>
          </div>
          <div class="clearfix"></div>
          <br/>
          <?php
          $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['Update'])):
            unset($post['Update']);
            $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
            $post['vaga_id'] = $get;

            $update = new Vagas();
            $update->ExeUpdate($post);

            if (!$update->getResult()):
              $erro = $update->getError();
              WSErro($erro[0], $erro[1], null, $erro[2]);
            else:
              $_SESSION['Error'] = $update->getError();
              header('Location: painel.php?exe=vagas/index&get=true');
            endif;
          endif;

          $ReadRecursos = new Read;
          $ReadRecursos->ExeRead(TB_VAGA, "WHERE user_empresa = :emp AND vaga_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$get}");
          if ($ReadRecursos->getResult()):
            foreach ($ReadRecursos->getResult() as $dados):
              ?>
              <div class="x_panel">
                <div class="x_content">
                  <div class="x_title">
                    <h2>Edite áreas ou categorias.</h2>                           
                    <div class="clearfix"></div>                            
                  </div>
                  <br/>                             

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vaga_title">Área ou vaga <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="text" id="vaga_title" name="vaga_title" required="required" class="form-control col-md-7 col-xs-12" placeholder="Digite o nome da área ou categoria" value="<?php
                      if (isset($post['vaga_title'])): echo $post['vaga_title'];
                      else: echo $dados['vaga_title'];
                      endif;
                      ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vaga_parent">O que seria o item acima? <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <select name="vaga_parent" class="form-control col-md-7 col-xs-12">
                        <?php
                        $ReadRecursos->ExeRead(TB_VAGA, "WHERE user_empresa = :emp AND vaga_parent IS NULL ORDER BY vaga_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}");
                        ?>
                        <option value="" class="text-info text-bold" style="padding: 10px;">Isto é uma sessão!</option>
                        <?php
                        if ($ReadRecursos->getResult()):
                          foreach ($ReadRecursos->getResult() as $key):
                            ?>
                            <option value="<?= $key['vaga_id'] ?>" <?php
                            if (isset($post['vaga_parent']) && $post['vaga_parent'] == $key['vaga_id']): echo 'selected="selected"';
                            elseif ($key['vaga_id'] == $dados['vaga_parent']): echo 'selected="selected"';
                            endif;
                            ?> style="padding:5px;">» <?= $key['vaga_title']; ?></option>
                                    <?php
                                    $ReadRecursos->ExeRead(TB_VAGA, "WHERE user_empresa = :emp AND vaga_parent = :cat ORDER BY vaga_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$key['vaga_id']}");
                                    foreach ($ReadRecursos->getResult() as $key):
                                      ?>
                              <option value="" disabled="disable" style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;<?= $key['vaga_title']; ?></option>
                              <?php
                            endforeach;
                          endforeach;
                        endif;
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vaga_content">Se for uma vaga, descreva a mesma com detalhes <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea name="vaga_content" class="form-control col-md-7 col-xs-12 j_word"><?php
                        if (isset($post['vaga_content'])): echo $post['vaga_content'];
                        else: echo $dados['vaga_content'];
                        endif;
                        ?></textarea>
                    </div>
                  </div>
                </div>
                <?php
              endforeach;
            endif;
            ?>
          </div>
        </div>
      </div>
    </div>
    <div class="pull-right">
      <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=vagas/index'"><i class="fa fa-home"></i></button>
    </div>
    <div class="clearfix"></div>
    <br/>
  </form>
  <div class="clearfix"></div>
</div>
