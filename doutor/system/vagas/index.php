<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-building-o"></i> Lista de áreas e vagas</h3>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12"> 
                <div class="x_panel">
                    <div class="x_content">
                        <div class="x_title">
                            <h2>Áreas e vagas.<small>Você também pode acessar, editar, remover e alterar status das vagas e áreas.</small></h2>                           
                            <div class="clearfix"></div>                            
                        </div>
                        <br/>                       
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                        unset($_SESSION['Error']);
                    endif;

                    $ReadRecursos = new Read;
                    $ReadRecursos->ExeRead(TB_VAGA, "WHERE user_empresa = :emp AND vaga_parent IS NULL ORDER BY vaga_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}");
                    if (!$ReadRecursos->getResult()):
                        WSErro("Desculpe mas não foi encontrado nenhuma área no sistema.", WS_INFOR, null, SITENAME);
                    else:
                        ?> 
                        <div class="clearfix"></div>
                        <!-- start accordion -->
                        <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php
                            foreach ($ReadRecursos->getResult() as $key):
                                extract($key);
                                ?>
                                <div class="panel j_item" id="<?= $vaga_id; ?>">
                                    <a class="panel-heading" role="tab" id="CatHeading<?= $vaga_id; ?>" data-toggle="collapse" data-parent="#accordion" href="#Cat-<?= $vaga_id; ?>" aria-expanded="true" aria-controls="Cat-<?= $vaga_id; ?>">
                                        <h4 class="panel-title">Área: <strong><?= $vaga_title; ?></strong></h4>
                                    </a>
                                    <div id="Cat-<?= $vaga_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="CatHeading<?= $vaga_id; ?>">
                                        <div class="panel-body">
                                            <div class="pull-left col-md-9">                                                
                                                <p><?= $vaga_content; ?></p>
                                            </div>                                            
                                            <div class="pull-right">                                                                                                
                                                <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=vagas/update&id=<?= $vaga_id; ?>'"><i class="fa fa-pencil"></i></button>
                                                <button type="button" class="btn btn-danger j_remove" rel="<?= $vaga_id; ?>" action="RemoveVaga"><i class="fa fa-trash"></i></button>
                                                <button type="button" class="btn j_statusRecursos" rel="<?= $vaga_id; ?>" tabindex="getStatusVaga" action="StatusVaga" value="<?= $vaga_status; ?>"><i class="fa fa-ban"></i></button>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr>
                                            <?php
                                            $ReadRecursos->ExeRead(TB_VAGA, "WHERE user_empresa = :emp AND vaga_parent = :cat ORDER BY vaga_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$vaga_id}");
                                            if (!$ReadRecursos->getResult()):
                                                WSErro("Nenhuma categoria foi encontrada para esta sessão.", WS_INFOR, NULL, SITENAME);
                                            else:
                                                ?>
                                            <div class="table-responsive">
                                                <table class="table table-striped dtr-inline jambo_table nowrap">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Vaga</th>                                                            
                                                            <th>Descrição</th>                                                            
                                                            <th>Status</th>
                                                            <th>Ações</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($ReadRecursos->getResult() as $cont):
                                                            extract($cont);
                                                            ?>
                                                            <tr class="j_item" id="<?= $vaga_id; ?>">
                                                                <th scope="row"><?= $vaga_id; ?></th>
                                                                <td><?= $vaga_title; ?></td>                                                                
                                                                <td><?= Check::Words($vaga_content, 10); ?></td>                                                               
                                                                <td class="j_GetStatusRecursos" rel="<?= $vaga_id; ?>"></td>                                                               
                                                                <td style="width: 110px;">
                                                                    <div class="btn-group btn-group-xs">                                                                                                                        
                                                                        <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=vagas/update&id=<?= $vaga_id; ?>'"><i class="fa fa-pencil"></i></button>
                                                                        <button type="button" class="btn btn-danger j_remove" rel="<?= $vaga_id; ?>" action="RemoveVaga"><i class="fa fa-trash"></i></button>
                                                                        <button type="button" class="btn j_statusRecursos" rel="<?= $vaga_id; ?>" tabindex="getStatusVaga" action="StatusVaga" value="<?= $vaga_status; ?>"><i class="fa fa-ban"></i></button>
                                                                    </div>                                                        
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>                                                
                                                </table>
                                            </div>
                                            <?php endif; ?> 
                                        </div>
                                    </div>  
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- end of accordion -->                     
                    <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
