<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <div class="page-title">
        <div class="title_left">
            <h3>Perfil</h3> 
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">   
                <?php
                $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                $ReadUser = new Read;
                $ReadUser->ExeRead(TB_USERS, "WHERE user_id = :id AND user_empresa = :emp", "id={$get}&emp={$_SESSION['userlogin']['user_empresa']}");
                if (!$ReadUser->getResult()):
                    WSErro("Desculpe, usuário não encontrado.", WS_ERROR, null, "Doutores da Web");
                else:
                    foreach ($ReadUser->getResult() as $userpf):
                        extract($userpf);
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="x_panel">
                                    <div class="pull-left media">
                                        <div class="media-left"><a href="javascript:void(0)" style="display: inline-block; border-radius: 50%; padding: 3px; background-color: #fff;"><?= Check::Image('uploads/' . $user_cover, $user_name, 'media-object img-circle'); ?></a></div>
                                        <div style="width: auto" class="media-body media-middle">
                                            <h2 class="media-heading"><?= $user_name; ?> <?= $user_lastname; ?></h2>
                                            <div class="fs-20"><?= $user_cargo; ?> - <strong class="text-warning"><?= $empresa_name; ?></strong></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <h2><?= $user_name; ?></h2>
                                    <p><?= $user_cargo ?></p>
                                    <p><?= date('d/m/Y', strtotime($user_nasc)); ?></p>
                                    <p><?= $user_email ?></p>
                                    <p><?= $empresa_endereco; ?> <?= $cidade_nome; ?>/<?= $cidade_uf; ?></p>
                                    <p><?= $user_fone ?></p>
                                    <p><?= $empresa_site; ?></p>

                                    <hr>
                                    <h2>Sobre a empresa</h2>
                                    <p><b>Ramo:</b> <?= $empresa_ramo; ?></p>
                                    <p><?= $empresa_sobre; ?></p>
                                </div>
                            </div>   
                            
                            <div class="col-md-6 col-xs-12">
                                <div class="x_panel">
                                    <h2>Outros usuários da empresa</h2>                                   
                                        <ul class="media-list mb-0">                                                                        
                                            <?php
                                            $usuarios = new Read;
                                            $cidadeUsers = new Read;

                                            $usuarios->ExeRead(TB_USERS, 'WHERE user_empresa = :empid AND user_id != :userid', "empid={$empresa_id}&userid={$user_id}");
                                            if ($usuarios->getResult()):
                                                foreach ($usuarios->getResult() as $usuers):
                                                    extract($usuers);
                                                    ?>
                                                    <li class="media">
                                                        <a href="painel.php?exe=profile/view&id=<?= $user_id; ?>" class="conversation-toggle j_viewUser">
                                                            <div class="media-left avatar"><?= Check::Image('../doutor/uploads/' . $user_cover, $user_name, 'media-object img-circle'); ?><span class="status bg-success"></span></div>
                                                            <div class="media-body">
                                                                <h5 class="media-heading"><?= $user_name; ?> <?= $user_lastname; ?></h5>
                                                                <p class="text-muted mb-0"><?= $empresa_endereco; ?> - <?= $cidade_nome; ?>/<?= $cidade_uf; ?></p>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>                                    
                                        </ul>
                                </div>
                            </div>                            
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>        
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /page content -->