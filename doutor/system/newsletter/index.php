<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 2;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>

  <div class="page-title">
    <div class="title col-md-12 col-sm-6 col-xs-12">
      <h3><i class="fa fa-database"></i> Lista de e-mails Newslleter</h3>
    </div>
    <div class="clearfix"></div>
    <br/>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">  
        <div class="x_panel"> 
          <div class="x_content">
            <?php
            $ReadCand = new Read;
            $ReadCand->ExeRead(TB_NEWSLETTER, "WHERE user_empresa = :emp ORDER BY news_data DESC", "emp={$_SESSION['userlogin']['user_empresa']}");
            if (!$ReadCand->getResult()):
              WSErro("Nenhum e-mail foi encontrado.", WS_INFOR, null, "Aviso!");
            else:
              ?>
              <div class="x_title">
                <h2>Confira a lista de e-mails cadastrados via newsletter no site.<small>Você também pode exportar as listas de e-mails.</small></h2>                           
                <div class="clearfix"></div>                            
              </div>
              <br/>                                                     
              <div class="table-responsive">
                <table id="datatable-buttons" class="table table-striped dtr-inline dataTable jambo_table nowrap">
                  <thead>
                    <tr>
                      <th>Ações</th>           
                      <th>Nome</th>                                
                      <th>E-mail</th>            
                      <th>Confirmado?</th>            
                      <th>Data</th>                               
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($ReadCand->getResult() as $dados):
                      extract($dados);
                      ?>
                      <tr class="j_item" id="<?= $news_id; ?>">
                        <td style="width: 70px;">
                          <div class="btn-group btn-group-xs">                                                                                                                                                                                                            
                            <button type="button" class="btn btn-danger j_remove" rel="<?= $news_id; ?>" action="RemoveEmailNews"><i class="fa fa-trash"></i></button>                                                    
                          </div>                                                        
                        </td>    
                        <td><?= $news_nome; ?></td>                                   
                        <td><?= $news_email; ?></td>                                
                        <td><?= GetStatusNews($news_status); ?></td>                                
                        <td><?= date("d/m/Y H:i", strtotime($news_data)); ?></td>                                            
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>                        
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

  </div>
  <div class="clearfix"></div>
</div>