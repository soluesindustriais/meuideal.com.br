<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 2;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
    <div class="page-title">
      <div class="title_left">
        <h3><i class="fa fa-list-ol"></i> Editar sessões e categorias</h3>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
          <div class="pull-right">
            <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=categorias/index'"><i class="fa fa-home"></i></button>
          </div>
          <div class="clearfix"></div>
          <br/>
          <?php
          $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['Update'])):
            unset($post['Update']);
            $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
            $post['cat_cover'] = ( $_FILES['cat_cover']['tmp_name'] ? $_FILES['cat_cover'] : null );
            $post['cat_id'] = $get;

            $update = new Categorias();
            $update->ExeUpdate($post);

            if (!$update->getResult()):
              $erro = $update->getError();
              WSErro($erro[0], $erro[1], null, $erro[2]);
            else:
              $_SESSION['Error'] = $update->getError();
              header('Location: painel.php?exe=categorias/index&get=true');
            endif;
          endif;

          $ReadRecursos = new Read;
          $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$get}");
          if ($ReadRecursos->getResult()):
            foreach ($ReadRecursos->getResult() as $dados):
              extract($dados);
              ?>
              <div class="x_panel">
                <div class="x_content">
                  <div class="x_title">
                    <h2>Edite sessões e categorias.</h2>                           
                    <div class="clearfix"></div>                            
                  </div>
                  <br/>                             

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_title">Sessão ou categoria <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="text" id="cat_title" name="cat_title" required="required" class="form-control col-md-7 col-xs-12" placeholder="Digite o nome da categoria ou sessão" value="<?php
                      if (isset($post['cat_title'])): echo $post['cat_title'];
                      else: echo $dados['cat_title'];
                      endif;
                      ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_parent">O que seria o item acima? <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <select name="cat_parent" class="form-control col-md-7 col-xs-12">
                        <?php
                        $ReadRecursos = new Read;
                        $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent IS NULL ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}");
                        ?>                                                
                        <option value="" class="text-info text-bold" style="padding: 10px;" <?php
                        if (isset($post['cat_parent']) && empty($post['cat_parent'])): echo 'selected="selected"';
                        endif;
                        ?>>Isto é uma sessão!</option>
                                <?php
                                if ($ReadRecursos->getResult()):
                                  foreach ($ReadRecursos->getResult() as $key):
                                    ?>
                            <option value="<?= $key['cat_id'] ?>" <?php
                            if (isset($post['cat_parent']) && $post['cat_parent'] == $key['cat_id'] || $key['cat_id'] == $dados['cat_parent']): echo 'selected="selected"';
                            endif;
                            ?> style="padding:5px">» <?= $key['cat_title']; ?></option>
                                    <?php
                                    $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$key['cat_id']}");
                                    foreach ($ReadRecursos->getResult() as $key):
                                      ?>
                              <option value="<?= $key['cat_id']; ?>" <?php
                              if (isset($post['cat_parent']) && $post['cat_parent'] == $key['cat_id'] || $key['cat_id'] == $dados['cat_parent']): echo 'selected="selected"';
                              endif;
                              if ($key['cat_id'] == $dados['cat_id']): echo 'disabled="disabled"';
                              endif;
                              ?> style="color: #999999; padding: 4px;">&nbsp;&nbsp;&nbsp;<?= $key['cat_title']; ?></option>
                                      <?php
                                      $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$key['cat_id']}");
                                      if ($ReadRecursos->getResult()):
                                        foreach ($ReadRecursos->getResult() as $keys):
                                          ?>
                                  <option value="<?= $keys['cat_id']; ?>" disabled="disabled" style="color: #ddd; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow; <?= $keys['cat_title']; ?></option>
                                  <?php
                                endforeach;
                              endif;
                            endforeach;
                          endforeach;
                        endif;
                        ?>
                      </select>
                    </div>                                       
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_cover">Capa da categoria, <span class="text-danger">ao reenviar, o arquivo antigo será substituído</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">                      

                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <button type="button" class="btn btn-app j_remove" rel="<?= $cat_id; ?>" action="RemoveCapaCategoria"><i class="fa fa-trash"></i> Remover capa</button>
                      </div>  
                      <div class="col-md-10 col-sm-10 col-xs-12 j_item" id="<?= $cat_id; ?>">
                        <?= Check::Image("../doutor/uploads/" . $cat_cover, $cat_title, 'img-responsive', 130); ?>
                      </div>
                      <div class="clearfix"></div>
                      <input type="file" id="app_cover" name="cat_cover">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_keywords">Keywords</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">                                                                
                      <input id="tags_1" type="text" name="cat_keywords" class="tags form-control col-md-7 col-xs-12" value="<?php
                      if (isset($post['cat_keywords'])): echo $post['cat_keywords'];
                      else: echo $cat_keywords;
                      endif;
                      ?>">
                      <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_description">Descrição, até 160 caractéres <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea name="cat_description" 
                                required="required" 
                                class="form-control col-md-7 col-xs-12 j_contChars" 
                                data-parsley-trigger="keyup" 
                                data-parsley-minlength="140" 
                                data-parsley-maxlength="160" 
                                data-parsley-minlength-message="Você deve digitar no mínimo 140 caracteres!" 
                                data-parsley-validation-threshold="139"><?php
                                  if (isset($post['cat_description'])): echo $post['cat_description'];
                                  else: echo $dados['cat_description'];
                                  endif;
                                  ?></textarea>
                      <div class="j_cont"></div> 
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_content">Conteúdo da página de categorias
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea name="cat_content" class="form-control col-md-7 col-xs-12 j_word"><?php
                        if (isset($post['cat_content'])): echo $post['cat_content'];
                        else: echo $cat_content;
                        endif;
                        ?></textarea>                                       
                    </div>
                  </div>

                </div>
              </div>
              <?php
            endforeach;
          endif;
          ?>

        </div>
      </div>
    </div>
    <div class="pull-right">
      <button type="submit" name="Update" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=categorias/index'"><i class="fa fa-home"></i></button>
    </div>
    <div class="clearfix"></div>
    <br/>
  </form>
  <div class="clearfix"></div>
</div>
