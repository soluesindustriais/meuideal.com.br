<?php

// CONEXÃO DO BANCO DE DADOS ####################
define('HOST', 'mysql942.umbler.com');
define('USER', 'rotab');
define('PASS', 'solucoes2018');
define('DBSA', 'sig2');

/* SITE CONFIG */
define('SITE_NAME', 'Meu Ideal'); //Nome do site do cliente
define('SITE_SUBNAME', 'Marketing'); //Slogan
define('SITE_DESC', 'Description principal do site, conteúdo de efeito com carro chefe do cliente.'); //Descrição do site do cliente até 160 caractéres

define('SITE_FONT_NAME', 'Lato'); //Tipografia do site (https://www.google.com/fonts)
define('SITE_FONT_WHIGHT', '400,300,700,900'); //Tipografia do site (https://www.google.com/fonts)

/*
 * DADOS DO SEU CLIENTE/DONO DO SITE
 */
define('SITE_ADDR_NAME', 'Grupo Ideal Trends'); //Nome de remetente
define('SITE_ADDR_RS', 'Grupo Ideal Trends LTDA'); //Razão Social
define('SITE_ADDR_EMAIL', 'contato@idealtrends.com.br'); //E-mail de contato
define('SITE_ADDR_SITE', 'http://www.idealtrends.com.br'); //URL descrita
define('SITE_ADDR_CNPJ', '00.000.000/0000-00'); //CNPJ da empresa
define('SITE_ADDR_IE', '000/0000000'); //Inscrição estadual da empresa
define('SITE_ADDR_PHONE_A', '(11) 9999-9999'); //Telefone 1
define('SITE_ADDR_PHONE_B', '(11) 0.0000-0000'); //Telefone 2
define('SITE_ADDR_ADDR', 'Rua Pequetita, 301'); //ENDEREÇO: rua, número (complemento)
define('SITE_ADDR_CITY', 'São Paulo'); //ENDEREÇO: cidade
define('SITE_ADDR_DISTRICT', 'Vila Olímpia'); //ENDEREÇO: bairro
define('SITE_ADDR_UF', 'SP'); //ENDEREÇO: UF do estado
define('SITE_ADDR_ZIP', '00000-000'); //ENDEREÇO: CEP
define('SITE_ADDR_COUNTRY', 'Brasil'); //ENDEREÇO: País
define('SITE_ADDR_COUNTRY_SIGLA', 'BR'); //ENDEREÇO: País
define('SITE_CORDENADAS', '');

/**
 * <b>Define o perfil do template</b>
 * Habilite e configure os widget ou APPs do template
 */
define("APP", true); //Ative ou desativa os APPs/Widgets do template

/*
 * WIDGETS
 * Ativação de carrinhos de orçamento, banners, newsletter etc... 
 */
if (APP):
  
  define("WIDGET_LANG", true); //false para desativar a lista de idiomas  
  define('WIDGET_CART', true); //ADICIONA OS BOTÕES E MODELOS DOS PRODUTOS PARA ADICIONAR AO CARRINHO
  define('WIDGET_SEARCH', true); //ADICIONA A BARRA DE PESQUISA
  define("WD_BANNER", false); //false para desativar e (int) para modelo do banner a ser aplicado    

  define("MIN_PER_PAGE", 25); //Quantidade mínima por página
  define("MULTIPLO_PAGE", 5); //Número de Selects das páginas multiplicado
  
endif;