<?php

/**
 * AdminUsers.class [ CLASSE ]
 * Classe responsabel pela administração de dos usuários, cadastro, atualização e exclusão
 * @copyright (c) 2016, Rafael da Silva Lima - Inove Dados
 */
class AdminUsers {

    //Tratamento de mensagens e resultados
    private $Result;
    private $Error;
    //Entrada de dados
    private $Data;
    private $User;
    //Montagem da mensagem de boas vindas
    private $Mensagem;

    public function ExeCreate(array $Data) {
        $this->Data = $Data;
        $this->CheckUnset();
        $this->CheckDataCreate();
        if ($this->Result):
            $this->Create();
        endif;
    }

    /**
     * <b>Atualizar Usuário:</b> Envelope os dados em uma array atribuitivo e informe o id de um
     * usuário para atualiza-lo no sistema!
     * @param INT $UserId = Id do usuário
     * @param ARRAY $Data = Atribuitivo
     */
    public function ExeUpdate($UserId, array $Data) {
        $this->User = (int) $UserId;
        $this->Data = $Data;
        $this->CheckUnset();
        $this->CheckDataUpdate();
        if ($this->Result):
            $this->Update();
        endif;
    }

    /**
     * <b>Retorno de consulta</b>
     * Se não houve consulta ele retorna false bolean
     * @return object = consulta do webservice envelopada
     */
    public function getResult() {
        return $this->Result;
    }

    /**
     * <b>Mensagens do sistema</b>
     * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
     * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
     */
    public function getError() {
        return $this->Error;
    }

    ########################################
    ########### METODOS PRIVADOS ###########
    ########################################
    //Metodo para verificar se os campos opcionais estão vazios e dar unset em sua chave no array.

    private function CheckUnset() {
        if (empty($this->Data['user_password']) && empty($this->Data['txtConfirmPassword'])):
            unset($this->Data['user_password'], $this->Data['txtConfirmPassword']);
        endif;

        if (empty($this->Data['user_fone'])):
            $this->Data['user_fone'] = ' ';            
        endif;

        if (empty($this->Data['user_ramal'])):
            $this->Data['user_ramal'] = ' ';
        endif;
    }

    //Verifica os dados entrados, organiza e cria keys faltantes
    private function CheckDataCreate() {

        if (in_array('', $this->Data)):
            $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        elseif (!Check::Email($this->Data['user_email'])):
            $this->Error = array("Digite um endereço de e-mail válido.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        elseif ($this->Data['user_level'] >= 5 && $_SESSION['userlogin']['user_level'] < 5):
            $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        else:
            $this->Data['user_nasc'] = Check::Data($this->Data['user_nasc']);
            $this->Data['user_registration'] = date('Y-m-d H:i:s');
            $this->CheckEmail();
        endif;
    }

    private function CheckDataUpdate() {
        if (in_array('', $this->Data)):
            $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        elseif (isset($this->Data['user_password']) && !empty($this->Data['user_password']) && empty($this->Data['txtConfirmPassword'])):
            $this->Error = array("Confirme sua senha para realizar as alterações.", WS_ALERT, "Doutores da Web");
            $this->Result = false;
        elseif (!$this->CheckPass()):
            $this->Error = array("As senhas não coincidem.", WS_ALERT, "Doutores da Web");
            $this->Result = false;
        elseif (!Check::Email($this->Data['user_email'])):
            $this->Error = array("Digite um endereço de e-mail válido.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        elseif ($this->Data['user_level'] >= 5 && $_SESSION['userlogin']['user_level'] < 5):
            $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        else:
            $this->Data['user_nasc'] = Check::Data($this->Data['user_nasc']);
            unset($this->Data['txtConfirmPassword']);
            $this->CheckEmail();
        endif;
    }

    //Verifica se as senhas são iguais
    private function CheckPass() {
        if ($this->Data['user_password'] != $this->Data['txtConfirmPassword']):
            return false;
        else:
            return true;
        endif;
    }

    //Verifica usuário pelo e-mail, Impede cadastro duplicado!
    private function CheckEmail() {
        $where = ( isset($this->User) ? "user_id != {$this->User} AND" : '' );
        $readUser = new Read;
        $readUser->ExeRead(TB_USERS, "WHERE {$where} user_email = :email", "email={$this->Data['user_email']}");
        if ($readUser->getResult()):
            $this->Error = array( "Digite um endereço de e-mail diferente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        else:
            $this->Result = true;
        endif;
    }

    //Cadastra o usuário
    private function Create() {
        $user_pass = $this->Data['user_password'];
        $this->Data['user_password'] = md5($user_pass);
        $cadUser = new Create;
        $cadUser->ExeCreate(TB_USERS, $this->Data);
        if (!$cadUser->getResult()):
            $this->Result = false;
            $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
        else:
            $this->Result = true;
            $this->Data['user_pass'] = $user_pass;
            $this->SendWelcome();
        endif;
    }

    //Atualiza Usuário!
    private function Update() {
        $Update = new Update;
        if (isset($this->Data['user_password'])):
            $user_pass = $this->Data['user_password'];
            $this->Data['user_password'] = md5($user_pass);
        else:
            $user_pass = 'Não alterada';
        endif;
        $Update->ExeUpdate(TB_USERS, $this->Data, "WHERE user_id = :id", "id={$this->User}");
        if ($Update->getResult()):
            $this->Error = array("O usuário <b>{$this->Data['user_name']}</b> foi atualizado com sucesso.", WS_ACCEPT, "Doutores da Web");
            $this->Result = true;
            $this->Data['user_pass'] = $user_pass;
            $this->Data['user_fone'] = ( isset($this->Data['user_fone']) ? $this->Data['user_fone'] : 'Não informado' );
            $this->Data['user_ramal'] = ( isset($this->Data['user_ramal']) ? $this->Data['user_ramal'] : 'Não informado' );
            $this->Data['user_nasc'] = date('d/m/Y', strtotime($this->Data['user_nasc']));
            $this->SendUpdate();
        endif;
    }

    //Envia e-mail de boas vindas para a empresa
    private function SendWelcome() {

        //Paramentos a serem enviados dentro do template view formato Array
        $this->Data['MENSAGEM'] = BOAS_VINDAS_USER;
        $this->Data['SITENAME'] = SITENAME;
        $this->Data['SITEDESC'] = SITEDESC;

        $view = new View;
        $tpl = $view->Load('email/cadastrousuario');

        $SendMail = new Email;
        
        //Define o cabeçalho e a mensagem do e-mail que pode ser vinda de um tpl
        $this->Mensagem['Assunto'] = 'Usuário cadastrado - ' . SITENAME;
        $this->Mensagem['DestinoNome'] = $this->Data['user_name'] . ' ' . $this->Data['user_lastname'];
        $this->Mensagem['DestinoEmail'] = $this->Data['user_email'];
        $this->Mensagem['RemetenteNome'] = REMETENTENOME;
        $this->Mensagem['RemetenteEmail'] = REMETENTEMAIL;
        $this->Mensagem['Mensagem'] = $view->Show($this->Data, $tpl);

        $SendMail->Enviar($this->Mensagem);

        if (!$SendMail->getResult()):
            $this->Error = array("<p>O usuário <b>{$this->Data['user_name']} {$this->Data['user_lastname']}</b> foi cadastrado com sucesso, porém não foi possível enviar o e-mail com os dados de acesso.</p>", WS_INFOR, "Doutores da Web");
            Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Usuários", "Cadastrou o usuário {$this->Data['user_name']} {$this->Data['user_lastname']}", date("Y-m-d H:i:s"));
        else:
            $this->Error = array("O usuário <b>{$this->Data['user_name']} {$this->Data['user_lastname']}</b> foi cadastrado com sucesso, um e-mail foi enviado com os dados de acesso.", WS_ACCEPT, "Doutores da Web");
            Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Usuários", "Cadastrou o usuário {$this->Data['user_name']} {$this->Data['user_lastname']}", date("Y-m-d H:i:s"));
            $this->Data = null;
            $this->Mensagem = null;
        endif;
    }

    //Envia e-mail de boas vindas para a empresa
    private function SendUpdate() {

        //Paramentos a serem enviados dentro do template view formato Array
        $this->Data['MENSAGEM'] = UPDATE_USER;
        $this->Data['SITENAME'] = SITENAME;
        $this->Data['SITEDESC'] = SITEDESC;

        $view = new View;
        $tpl = $view->Load('email/updateusuario');
        
        $SendMail = new Email;

        //Define o cabeçalho e a mensagem do e-mail que pode ser vinda de um tpl
        $this->Mensagem['Assunto'] = 'Usuário atualizado - ' . SITENAME;
        $this->Mensagem['DestinoNome'] = $this->Data['user_name'] . ' ' . $this->Data['user_lastname'];
        $this->Mensagem['DestinoEmail'] = $this->Data['user_email'];
        $this->Mensagem['RemetenteNome'] = REMETENTENOME;
        $this->Mensagem['RemetenteEmail'] = REMETENTEMAIL;
        $this->Mensagem['Mensagem'] = $view->Show($this->Data, $tpl);

        $SendMail->Enviar($this->Mensagem);

        if (!$SendMail->getResult()):
            $this->Error = array("<p>O usuário <b>{$this->Data['user_name']} {$this->Data['user_lastname']}</b> foi atualizado com sucesso, porém não foi possível enviar o e-mail com os dados atualizados.</p>", WS_INFOR, "Doutores da Web");
            Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Usuários", "Atualizou o usuário {$this->Data['user_name']} {$this->Data['user_lastname']}", date("Y-m-d H:i:s"));
        else:
            $this->Error = array("O usuário <b>{$this->Data['user_name']} {$this->Data['user_lastname']}</b> foi atualizado com sucesso, um e-mail foi enviado com os dados atualizados.", WS_ACCEPT, "Doutores da Web");
            Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Usuários", "Atualizou o usuário {$this->Data['user_name']} {$this->Data['user_lastname']}", date("Y-m-d H:i:s"));
            $this->Data = null;
            $this->Mensagem = null;
        endif;
    }

}
