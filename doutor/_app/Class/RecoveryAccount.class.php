<?php

/**
 * RecoveryAccount.class.php [CLASSE]
 * Classe responsável por recuperar a senha e validar os dados recuperados
 * @copyright (c) 2016, Rafael da Silva Lima - Inove Dados
 */
 
class RecoveryAccount {

    private $Data;
    private static $Email;
    private static $Mensagem;
    private $Result;
    private $Error;

    /**
     * <b>Responsavel por validar o e-mail e enviar as informações de recuperação</b>
     * @param array $Data = Dados envelopados em array user_email => email
     */
    public function ExeRecovery(array $Data) {
        $this->Data = $Data;        
        if (in_array("", $this->Data)):
            $this->Error = array("Digite um endereço de e-mail válido.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        elseif (!Check::Email($this->Data['user_email'])):
            $this->Error = array("Digite um endereço de e-mail válido.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        elseif (!$this->CheckEmail()):
            $this->Error = array("Cadastro não encontrado.", WS_ERROR, "Doutores da Web");
        else:
            $this->SetKey();
        endif;
    }

    public function ExeUpdate(array $Data) {
        $this->Data = $Data;
        if (in_array("", $this->Data)):
            $this->Error = array("E-mail e senha são obrigatórios.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        elseif (!Check::Email($this->Data['user_email'])):
            $this->Error = array("Digite um e-mail válido", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        elseif (!$this->CheckToken()):
            $this->Error = array("Cadastro não encontrado.", WS_ERROR, "Doutores da Web");
        else:
            $this->Update();
        endif;
    }

    public function getResult() {
        return $this->Result;
    }

    public function getError() {
        return $this->Error;
    }

    //Verifica se o e-mail existe no banco baseado no e-mail digitado.
    private function CheckEmail() {
        $readUser = new Read;
        $readUser->ExeRead(TB_USERS, "WHERE user_email = :email", "email={$this->Data['user_email']}");
        if ($readUser->getResult()):
            $read = $readUser->getResult();
            self::$Email = $read[0];
            return true;
        endif;
    }

    //Verifica se o e-mail existe no banco baseado no token.
    private function CheckToken() {
        $readUser = new Read;
        $readUser->ExeRead(TB_USERS, "WHERE user_key = :key", "key={$this->Data['user_key']}");
        if ($readUser->getResult()):
            $read = $readUser->getResult();
            $this->Data["user_id"] = $read[0]["user_id"];
            return true;
        endif;
    }

    //Cria o Token salva no banco e armazena no atributo static
    private function SetKey() {
        $key = array("user_key" => substr(md5(time() + self::$Email['user_email']), 0, 25));        
        $Update = new Update;
        $Update->ExeUpdate(TB_USERS, $key, "WHERE user_id = :id", "id=" . self::$Email['user_id']);
        if (!$Update->getResult()):
            $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        else:
            $readUser = new Read;
            $readUser->ExeRead(TB_USERS, "WHERE user_email = :email", "email={$this->Data['user_email']}");
            if ($readUser->getResult()):
                $read = $readUser->getResult();
                self::$Email = $read[0];
                $this->SendRecovery();
            endif;

        endif;
    }

    private function Update() {        
        self::$Email['user_password'] = md5($this->Data['user_password']);
        self::$Email['user_key'] = null;
        $Update = new Update;
        $Update->ExeUpdate(TB_USERS, self::$Email, "WHERE user_id = :id", "id={$this->Data['user_id']}");
        if (!$Update->getResult()):
            $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        else:
            $this->Result = true;
        endif;
    }

    //Pega as informações do usuário
    private function SendRecovery() {
        unset($this->Data);
        //Paramentos a serem enviados dentro do template view formato Array
        self::$Email['MENSAGEM'] = REC_SENHA;
        self::$Email['SITENAME'] = SITENAME;
        self::$Email['SITEDESC'] = SITEDESC;

        $view = new View;
        $tpl = $view->Load('email/recsenha');

        $SendMail = new Email;
        
        //Define o cabeçalho e a mensagem do e-mail que pode ser vinda de um tpl
        self::$Mensagem['Assunto'] = 'Recuperar senha - Doutores da Web';
        self::$Mensagem['DestinoNome'] = self::$Email['user_name'] . ' ' . self::$Email['user_lastname'];
        self::$Mensagem['DestinoEmail'] = self::$Email['user_email'];
        self::$Mensagem['RemetenteNome'] = REMETENTENOME;
        self::$Mensagem['RemetenteEmail'] = REMETENTEMAIL;
        self::$Mensagem['Mensagem'] = $view->Show(self::$Email, $tpl);

        $SendMail->Enviar(self::$Mensagem);
        
        if (!$SendMail->getResult()):
            $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        else:
            $this->Error = array("Enviamos as informações de recuperação de senha para o e-mail informado.", WS_ACCEPT, "Doutores da Web");
            Check::SaveHistoric(self::$Email['user_id'], self::$Email['user_cargo'], self::$Email['user_level'], "Esqueceu a senha", "O usuário com e-mail " . self::$Email['user_email'] . " solicitou a recuperação de senha", date("Y-m-d H:i:s"), self::$Email['user_empresa']);
            $this->Result = true;
        endif;
    }
}
