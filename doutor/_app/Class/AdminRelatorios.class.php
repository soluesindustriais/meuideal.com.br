<?php

/**
 * AdminRelatorios [ CLASSE ]
 * Classe resposável por gerir os Relatórios de consultoria para as emrpesas
 * @copyright (c) 2016, Rafael da Silva Lima - Inove Dados
 */
class AdminRelatorios {

    //Tratamento de resultados e mensagens
    private $Result;
    private $Error;
    //Entrada de dados
    private $Data;
    private $File;

    /**
     * <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
     * @param array $Data
     */
    public function ExeCreate(array $Data) {
        $this->Data = $Data;

        $this->CheckUnset();
        $this->CheckData();
        if ($this->Result):
            $this->Create();
        endif;
    }

    public function ExeUpdate(array $Data) {
        $this->Data = $Data;

        $this->CheckUnset();
        $this->CheckData();
        if ($this->Result):
            $this->CheckCover();
            $this->Update();
        endif;
    }

    /**
     * <b>Retorno de consulta</b>
     * Se não houve consulta ele retorna true boleano ou false para erros
     */
    public function getResult() {
        return $this->Result;
    }

    /**
     * <b>Mensagens do sistema</b>
     * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
     * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
     */
    public function getError() {
        return $this->Error;
    }

    ########################################
    ########### METODOS PRIVADOS ###########
    ########################################
    //Verifica se algum campo que não é obrigatorio está vazio e remove do array

    private function CheckUnset() {
        if (empty($this->Data['relat_obs'])):
            unset($this->Data['relat_obs']);
        endif;
        if (empty($this->Data['relat_file'])):
            unset($this->Data['relat_file']);
        endif;
    }

    //Verifica a integridade dos dados e direciona as operações
    private function CheckData() {
        if (in_array('', $this->Data)):
            $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        else:
            $this->File = ( isset($this->Data['relat_file']) ? $this->Data['relat_file'] : null );
            $this->Data['relat_datarelat'] = Check::Data($this->Data['relat_datarelat']);
            $this->Data['relat_datacad'] = date('Y-m-d H:s:i');
            $this->Data['relat_ano'] = (int) date("Y", strtotime($this->Data['relat_datarelat']));
            $this->CheckFile();
        endif;
    }

    //Verifica a consitência do arquivo e faz o upload
    private function CheckFile() {
        if (!empty($this->File)):
            $Upload = new Upload;
            $FileName = "relatorio-{$this->Data['relat_datacad']}-pdf-" . (substr(md5(time() + $this->Data['relat_datacad']), 0, 5));
            $Upload->File($this->File, $FileName, $this->Data['user_empresa'], 'relatorios');
            if (!$Upload->getResult()):
                $this->Error = array($Upload->getError(), WS_ERROR, 'Doutores da Web');
                $this->Result = false;
            else:
                $this->Data['relat_file'] = $Upload->getResult();
                $this->Result = true;
            endif;
        else:
            $this->Result = true;
        endif;
    }

    //Cadastra os dados do relatorio no banco
    private function Create() {
        $Create = new Create;
        $Create->ExeCreate(TB_RELATORIOS, $this->Data);
        if (!$Create->getResult()):
            $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
            $this->Result = false;
        else:
            $this->Result = true;
            $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Doutores da Web");
            Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Relatório de consultoria", "Cadastrou o relatório de consultoria ID #{$Create->getResult()} para a data de " . date("d/m/Y", strtotime($this->Data['relat_datarelat'])), date("Y-m-d H:i:s"));
            $this->Data = null;
            $this->File = null;
        endif;
    }

    //
    private function Update() {
        $Update = new Update;
        $Update->ExeUpdate(TB_RELATORIOS, $this->Data, "WHERE relat_id = :id", "id={$this->Data['relat_id']}");
        if (!$Update->getResult()):
            $this->Result = false;
        else:
            $this->Result = true;
            Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Relatório de consultoria", "Atualizou o relatório ID #{$this->Data['relat_id']} De: " . date("d/m/Y", strtotime($this->Data['relat_datarelat'])), date("Y-m-d H:i:s"));
            $this->Error = array("O relatório de <b>" . date("d/m/Y", strtotime($this->Data['relat_datarelat'])) . "</b>, foi atualizado com sucesso.", WS_ACCEPT, "Doutores da Web");
        endif;
    }

    //Verifica se já existe um arquivo e deleta para dar entrada ao novo
    private function CheckCover() {
        if (!empty($this->File)):
            $readFile = new Read;
            $readFile->FullRead("SELECT relat_file FROM " . TB_RELATORIOS . " WHERE relat_id = :id", "id={$this->Data['relat_id']}");
            if ($readFile->getRowCount()):
                $deleta = $readFile->getResult()[0]['relat_file'];
                if (file_exists("uploads/{$deleta}") && !is_dir("uploads/{$deleta}")):
                    unlink("uploads/{$deleta}");
                endif;
            endif;
        endif;
    }

}
