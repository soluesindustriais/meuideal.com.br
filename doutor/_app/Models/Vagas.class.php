<?php

/**
 * Vagas.class.php [MODEL]
 * Classe responsável por gerir as vagas dos candidatos no banco de dados.
 * @copyright (c) 2016, Rafael da Silva Lima & Doutores da Web
 */
class Vagas {

  //Tratamento de resultados e mensagens
  private $Result;
  private $Error;
  //Entrada de dados
  private $Data;
  private $Id = null;

  /**
   * <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
   * @param array $Data
   */
  public function ExeCreate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();
    $this->CheckData();

    if ($this->Result):
      $this->Create();
    endif;
  }

  public function ExeUpdate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();

    $this->Id = $this->Data['vaga_id'];
    unset($this->Data['vaga_id']);

    $this->CheckData();

    if ($this->Result):
      $this->Update();
    endif;
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }

  ########################################
  ########### METODOS PRIVADOS ###########
  ########################################
  //Verifica se algum campo que não é obrigatorio está vazio e remove do array

  private function CheckUnset() {
    if (empty($this->Data['vaga_parent'])):
      $this->Data['vaga_parent'] = " ";
    endif;
    if (empty($this->Data['vaga_content'])):
      $this->Data['vaga_content'] = " ";
    endif;
  }

  //Verifica a integridade dos dados e direciona as operações
  private function CheckData() {
    if (in_array('', $this->Data)):
      $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif ($this->CheckCat()):
      $this->Error = array("Já existe uma área ou vaga com este mesmo nome, tente outro", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->SetNull();
      $this->Result = true;      
    endif;
  }

  //Verifica os campos que estão vazios e seta eles como null para gravar no banco
  private function SetNull() {
    $this->Data = array_map('trim', $this->Data);
    foreach ($this->Data as $key => $value):
      if (empty($this->Data[$key])):
        $this->Data[$key] = null;
      endif;
    endforeach;
  }

  //Verifica se existem itens repetidos
  private function CheckCat() {
    $this->Data['vaga_name'] = Check::Name($this->Data['vaga_title']);
    $WHERE = (!empty($this->Id) ? "vaga_id != {$this->Id} AND" : null);
    $Read = new Read;
    $Read->ExeRead(TB_VAGA, "WHERE {$WHERE} vaga_name = :cat AND user_empresa = :emp", "cat={$this->Data['vaga_name']}&emp={$_SESSION['userlogin']['user_empresa']}");
    if ($Read->getResult()):
      return true;
    else:
      return false;
    endif;
  }

  //Cadastra os dados no banco
  private function Create() {
    $Create = new Create;
    $Create->ExeCreate(TB_VAGA, $this->Data);
    if (!$Create->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Vagas", "Cadastrou a vaga {$this->Data['vaga_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
    endif;
  }

  private function Update() {
    $Update = new Update;
    $Update->ExeUpdate(TB_VAGA, $this->Data, "WHERE user_empresa = :emp AND vaga_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$this->Id}");
    if (!$Update->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->Error = array("Cadastro atualizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Vagas", "Atualizou a vaga {$this->Data['vaga_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
      $this->Id = null;
    endif;
  }

}
