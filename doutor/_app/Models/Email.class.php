<?php

//require('_app/Library/PHPMailer/PHPMailerAutoload.php');

/**
 * Email.class.php [ MODEL ]
 * Model responsável por configurar a PHPMailer, validar os dados e dispara e-mails do sistema!
 * @copyright (c) 2016, Rafael da Silva Lima Web Stylus
 */
class Email {

  /** @var PHPMailer 
   * CHAMADA DOS METODOS DA CLASSE PRO NETBEANS LISTAR.
   */
  private $Mail;

  /** E-MAIL DATA */
  private $Data;

  /** CORPO DO E-MAIL */
  private $Assunto;
  private $Mensagem;

  /** REMETENTE */
  private $RemetenteNome;
  private $RemetenteEmail;

  /** DESTINO */
  private $DestinoNome;
  private $DestinoEmail;

  /** CONTROLE */
  private $Result;
  private $Error;

  /*
   * Inicializa o objeto setando os atributos herdados da classe PHPMailer e das definições do config.inc.php
   */

  function __construct() {
    //Solicita ao servidor os dados de SMTP
    $this->Crendentials();

    $this->Mail = new PHPMailer;
    $this->Mail->Host = MAILHOST;
    $this->Mail->Port = MAILPORT;
    $this->Mail->Username = MAILUSER;
    $this->Mail->Password = MAILPASS;
    $this->Mail->CharSet = 'utf-8';
    $this->Mail->SMTPSecure = 'ssl';
    $this->Mail->AltBody = 'Para ver este e-mail corretamente ative a visualização de HTML';
  }

  /**
   * <b>Método responsavel por enviar o e-mail</b>
   * @param array $Data = Dados recebido em array.
   */
  public function Enviar(array $Data) {
    $this->Data = $Data;
    $this->Clear();

    if (in_array('', $this->Data)):
      $this->Error = array("Erro ao enviar a mensagem: Para enviar este e-mail, preencha os campos requisitados.", WS_ALERT);
      $this->Result = false;
    elseif (!Check::Email($this->Data['RemetenteEmail'])):
      $this->Error = array("Erro ao enviar a mensagem: O e-mail que você informou não tem um formato válido. Informe seu E-mail.", WS_ALERT);
      $this->Result = false;
    else:
      $this->setEmail();
      $this->setConfig();
      $this->sendMail();
    endif;
  }

  public function getResult() {
    return $this->Result;
  }

  public function getError() {
    return $this->Error;
  }

  ############################################################
  #################### MÉTODOS PRIVATES ######################
  ############################################################

  private function Crendentials() {
    ini_set("max_execution_time", 60000);
    $verify = array('Hash' => openssl_encrypt("d640a8a4f9927b7ffa03126ced5f87155027870e", 'DES-EDE3-CBC', 'c17055ac4e99f9d95f4fc5a9edce2b45c6253d35', 0, '07bcc012'));

    //Envia os dados via cURL
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, 'http://doutor.mpisistema.com.br/_cdn/remoto/acessos.php');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $verify);
    $resposta = curl_exec($ch);
    curl_close($ch);

    //Resposta da requisição
    if ($resposta):
      $jSon = json_decode(str_replace("'", '"', $resposta));
      define('REMETENTENOME', SITENAME);
      define('MAILPORT', '465'); //Sempre usar Porta SSL
      define('MAILHOST', openssl_decrypt($jSon->Host, 'DES-EDE3-CFB', 'f2d714d6350052ad92be362462084f18950a4f57'));
      define('REMETENTEMAIL', openssl_decrypt($jSon->From, 'AES-256-OFB', 'fb3a027a67cd0714cc77e2590c0e27fbb9b9f888'));
      define("MAILUSER", openssl_decrypt($jSon->Username, 'DES-EDE3-OFB', 'ed2d8748242b0b22d4cd84e5f28ef2e3ad958aa3'));
      define('MAILPASS', openssl_decrypt($jSon->Password, 'DES-EDE3-CFB8', '4c68e9fc946992d07d7eaaf572df80e6105d987b'));
    endif;
  }

  /**
   * Método resposável por limpar códigos dos dados recebidos em array e remover os espaços.
   */
  private function Clear() {
    array_map('strip_tags', $this->Data);
    array_map('trim', $this->Data);
  }

  /**
   * Método responsabel por definicar os indices de cada atributo recebidos em array.
   */
  private function setEmail() {
    $this->Assunto = $this->Data['Assunto'];
    $this->Mensagem = $this->Data['Mensagem'];
    $this->RemetenteNome = $this->Data['RemetenteNome'];
    $this->RemetenteEmail = $this->Data['RemetenteEmail'];
    $this->DestinoNome = $this->Data['DestinoNome'];
    $this->DestinoEmail = $this->Data['DestinoEmail'];

    $this->Data = null;
    $this->setMsg();
  }

  /**
   * Método para setar templates de mensagens e tipos de e-mails que serão enviados
   */
  private function setMsg() {
    $this->Mensagem = "{$this->Mensagem}<hr><small>Enviada em: " . date('d/m/Y H:i:s') . "</small>";
  }

  /**
   * Método para definir as configuraões do e-mail
   */
  private function setConfig() {
    //SMTP AUTH
    $this->Mail->isSMTP();
    $this->Mail->SMTPAuth = true;
    $this->Mail->isHTML();
    $this->Mail->SMTPOptions = array(
      'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
      )
    );

    //REMETENTE E RETORNO
    $this->Mail->From = REMETENTEMAIL;
    $this->Mail->FromName = $this->RemetenteNome;
    $this->Mail->Sender = REMETENTEMAIL;
    $this->Mail->addReplyTo($this->RemetenteEmail, $this->RemetenteNome);

    //ASSUNTO, MENSAGEM E DESTINO
    $this->Mail->Subject = $this->Assunto;
    $this->Mail->Body = $this->Mensagem;
    $this->Mail->addAddress($this->DestinoEmail, $this->DestinoNome);
  }

  /**
   * Método responsável por fazer o envio do e-mail já tratado
   */
  private function sendMail() {
    if ($this->Mail->send()):
      $this->Error = array('Obrigado por entrar em contato: Recebemos sua mensagem e estaremos respondendo em breve', WS_ACCEPT, 'Aviso!');
      $this->Result = true;
    else:
      $this->Error = array("Erro ao enviar: Entre em contato com o WebMaster do site. ( {$this->Mail->ErrorInfo} )", WS_ACCEPT, 'Alerta!');
      $this->Result = false;
    endif;
  }

}
