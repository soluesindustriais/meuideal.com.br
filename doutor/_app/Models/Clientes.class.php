<?php

/**
 * Clientes.class.php [MODEL]
 * Classe responsável por gerir os clientes no banco de dados.
 * @copyright (c) 2016, Rafael da Silva Lima & Doutores da Web
 */
class Clientes {

  //Tratamento de resultados e mensagens
  private $Result;
  private $Error;
  //Entrada de dados
  private $Data;
  private $Id = null;

  /**
   * <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
   * @param array $Data
   */
  public function ExeCreate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();
    $this->CheckData();

    if ($this->Result):
      $this->Create();
    endif;
  }

  public function ExeUpdate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();

    $this->Id = $this->Data['cli_id'];
    unset($this->Data['cli_id']);

    $this->CheckData();

    if ($this->Result):
      $this->Update();
    endif;
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }

  ########################################
  ########### METODOS PRIVADOS ###########
  ########################################
  //Verifica se algum campo que não é obrigatorio está vazio e remove do array

  private function CheckUnset() {
    if (empty($this->Data['cli_status'])):
      unset($this->Data['cli_status']);
    endif;
    if (empty($this->Data['cli_cover'])):
      unset($this->Data['cli_cover']);
    endif;
    if (empty($this->Data['cli_description'])):
      $this->Data['cli_description'] = " ";
    endif;

    if (empty($this->Data['cli_url'])):
      $this->Data['cli_url'] = ' ';
    endif;
  }

  //Verifica a integridade dos dados e direciona as operações
  private function CheckData() {
    if (in_array('', $this->Data)):
      $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif (Check::CategoryRepeat($this->Data['cli_title'], $_SESSION['userlogin']['user_empresa'])):
      $this->Error = array("Já existe uma categoria ou sessão com o titulo: <b>{$this->Data['cli_title']}</b>, tente utilizar outro titulo.", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif ($this->CheckNome()):
      $this->Error = array("Já existe um cliente com este mesmo nome, tente outro", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:      
      $this->sendFoto();
      $this->SetNull();
    endif;
  }

  //Verifica os campos que estão vazios e seta eles como null para gravar no banco
  private function SetNull() {
    $this->Data = array_map('trim', $this->Data);
    foreach ($this->Data as $key => $value):
      if (empty($this->Data[$key])):
        $this->Data[$key] = null;
      endif;
    endforeach;
  }

  //Verifica se existem itens repetidos
  private function CheckNome() {
    $this->Data['cli_name'] = Check::Name($this->Data['cli_title']);
    $WHERE = (!empty($this->Id) ? "cli_id != {$this->Id} AND" : null);
    $Read = new Read;
    $Read->ExeRead(TB_CLIENTE, "WHERE {$WHERE} cli_name = :cat AND user_empresa = :emp", "cat={$this->Data['cli_name']}&emp={$_SESSION['userlogin']['user_empresa']}");
    if ($Read->getResult()):
      return true;
    else:
      return false;
    endif;
  }

  //Cadastra os dados no banco
  private function Create() {
    $Create = new Create;
    $Create->ExeCreate(TB_CLIENTE, $this->Data);
    if (!$Create->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Clientes", "Cadastrou o cliente {$this->Data['cli_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
    endif;
  }

  //Verifica e envia a foto do perfil do usuário para pasta da (int) empresa/user
  private function sendFoto() {
    if (!empty($this->Data['cli_cover']['tmp_name'])):

      $this->checkCover();
      $Upload = new Upload;
      $ImgName = "cliente-{$this->Data['cli_name']}-" . (substr(md5(time() + $this->Data['cli_title']), 0, 10));
      $Upload->Image($this->Data['cli_cover'], $ImgName, 500, $_SESSION['userlogin']['user_empresa'], 'clientes');
      if ($Upload->getError()):
        $this->Error = array($Upload->getError(), WS_ERROR, 'Alerta!');
        $this->Result = false;
      else:
        $this->Data['cli_cover'] = $Upload->getResult();
        $this->Result = true;
      endif;
    else:
      $this->Result = true;
    endif;
  }

  //Verifica se já existe uma foto, se sim deleta para enviar outra!
  private function checkCover() {
    if (!empty($this->Id)):
      $readCapa = new Read;
      $readCapa->ExeRead(TB_CLIENTE, "WHERE cli_id = :id AND user_empresa = :emp", "id={$this->Id}&emp={$_SESSION['userlogin']['user_empresa']}");
      $capa = $readCapa->getResult();
      if ($readCapa->getResult()):
        $delCapa = $capa[0]['cli_cover'];
        if (file_exists("uploads/{$delCapa}") && !is_dir("uploads/{$delCapa}")):
          unlink("uploads/{$delCapa}");
        endif;
      endif;
    endif;
  }

  private function Update() {
    $Update = new Update;
    $Update->ExeUpdate(TB_CLIENTE, $this->Data, "WHERE user_empresa = :emp AND cli_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$this->Id}");
    if (!$Update->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->Error = array("Cadastro atualizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Clientes", "Atualizou o cliente {$this->Data['cli_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
      $this->Id = null;
    endif;
  }

}
