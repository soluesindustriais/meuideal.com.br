<?php

// CONFIGRAÇÕES DO SITE ####################
define('APP_USERS', true); //true libera o acesso, false bloqueia o acesso de todos MODOS DE MANUTENÇÃO.
//DIFINE IDENTIDADE DO SITE ################
define('SITENAME', 'Doutores da Web');
define('SITEDESC', 'Sistema interno de gerenciamento');

//DEFINE A BASE DO SITE ####################
define('SYSROOT', '/projetos/meuideal.com.br'); // COLOCAR O /diretorio ou deixar vazio
define('BASE', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . SYSROOT . '/doutor');
define('RAIZ', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . SYSROOT);
define('HTACCESS', str_replace('www.', '', $_SERVER['HTTP_HOST']) . SYSROOT);

//CONFIGURAÇÕES DO CLIENTE
include_once('Config/Client.inc.php');

//DIFINIÇÃO DAS TABELAS prefixo app_ são para formação de widgets e dr_ são utilização geral
define('TB_EMP', 'app_empresas');
define('TB_CID', 'app_cidades');
define('TB_UF', 'app_estados');
define('TB_USERS', 'app_users');
define('TB_GALLERY', 'app_gallery');
define('TB_HISTORICO', 'app_historico');
define('TB_VAGA', 'app_vagas');
define('TB_CANDIDATO', 'app_candidatos');
define('TB_QUEMSOMOS', 'dr_quemsomos');
define('TB_ORCAMENTOS', 'dr_orcamento');
define('TB_RELATORIOS', 'dr_relatorios');
define('TB_NOTAS', 'dr_notas');
define('TB_CATEGORIA', 'dr_categoria');
define('TB_NOTICIA', 'dr_noticia');
define('TB_BLOG', 'dr_blog');
define('TB_PAGINA', 'dr_paginas');
define('TB_PRODUTO', 'dr_produtos');
define('TB_SERVICO', 'dr_servicos');
define('TB_EMPRESA', 'dr_empresas');
define('TB_BANNER', 'dr_sliders');
define('TB_CLIENTE', 'dr_clientes');
define('TB_CASE', 'dr_cases');
define('TB_DOWNLOAD', 'dr_downloads');
define('TB_CONFIG', 'app_config');
define('TB_NEWSLETTER', 'app_newsletter');
define('TB_PAISES', 'app_paises');
define('TB_HOME', 'dr_home');

//TEMA DO SITE
define('THEME', 'base');
define('INCLUDE_PATH', RAIZ . '/themes/' . THEME);
define('REQUIRE_PATH', 'themes' . DIRECTORY_SEPARATOR . THEME);

//Empresa Master Doutores e Empresa Cliente, consultar no banco ou na lista de empresas cadastradas
define("EMPRESA_MASTER", 1);
define("EMPRESA_CLIENTE", Check::SetIdioma(2));

// AUTO LOAD DE CLASSES ####################
function __autoload($Class) {
  $cDir = array('Conn', 'Helpers', 'Models', 'Library/PHPMailer', 'Class');
  $iDir = null;

  foreach ($cDir as $dirName):
    if (!$iDir && file_exists(__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . ".class.php") && !is_dir(__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . ".class.php")):
      include_once (__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . ".class.php");
      $iDir = true;
    endif;
  endforeach;

  if (!$iDir):
    WSErro("Não foi possível incluir {$Class}.class.php", WS_ERROR, null, "Doutor Web");
    die;
  endif;
}

//INCLUDES COM COMPLEMENTOS
include_once('Config/Mensagens.inc.php');
include_once('Config/Functions.inc.php');
include_once('Config/Agency.inc.php');

// TRATAMENTO DE ERROS #####################
//CSS constantes :: Mensagens de Erro
define('WS_NULL', 'default');
define('WS_ACCEPT', 'success');
define('WS_INFOR', 'info');
define('WS_ALERT', 'warning');
define('WS_ERROR', 'danger');

//WSErro :: Exibe erros lançados :: Front
function WSErro($ErrMsg, $ErrNo, $ErrDie = null, $ErrTitle = null) {
  $CssClass = ( $ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
  $TitleMsg = (!empty($ErrTitle) ? $ErrTitle : 'Mensagem' );
  echo "<div class=\"j_close panel panel-{$CssClass}\"><div class=\"panel-heading\"><h3 class=\"panel-title\">{$TitleMsg}</h3></div><div class=\"panel-body\">{$ErrMsg}</div></div>";

  if ($ErrDie):
    die;
  endif;
}

//PHPErro :: personaliza o gatilho do PHP
function PHPErro($ErrNo, $ErrMsg, $ErrFile, $ErrLine) {
  $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : WS_NULL)));
  echo "<div class=\"panel panel-{$CssClass}\"><div class=\"panel-heading\"><h3 class=\"panel-title\">Erro na Linha: #{$ErrLine} ::</h3></div><div class=\"panel-body\"><p>{$ErrMsg}</p><p>{$ErrFile}</p></div></div>";

  if ($ErrNo == E_USER_ERROR):
    die;
  endif;
}

//set_error_handler('PHPErro');
