<?php

/**
 * Seo [ MODEL ]
 * Classe de apoio para o modelo LINK. Pode ser utilizada para gerar SSEO para as páginas do sistema!
 * 
 * @copyright (c) 2016, Rafael da Silva Lima
 */
class Seo {
  /* Entrada de dados */

  private $Pach;
  private $File;
  private $Link;

  /* Tratamento */
  private $keywords;
  private $Schema;
  private $Title;
  private $Description;
  private $Image;

  public function __construct($Pach) {
    $this->Pach = explode('/', strip_tags(trim($Pach)));
    $this->File = end($this->Pach);

    $this->setPach();
  }

  public function getSchema() {
    return $this->Schema;
  }

  public function getTitle() {
    return $this->Title;
  }

  public function getDescription() {
    return $this->Description;
  }

  public function getkeywords() {
    return $this->keywords;
  }

  public function getImage() {
    return $this->Image;
  }

  /*
   * ***************************************
   * **********  PRIVATE METHODS  **********
   * ***************************************
   * Tratamento do Patch de acordo com a url, utilizando como referência
   * array[0] do patch para localizar o modulo 
   * end(array[]) para a URL final da página atual
   */

  private function setPach() {

    $Read = new Read;

    if ($this->File == 'index'):
      //INDEX
      $this->Schema = 'WebSite';
      $this->Title = SITE_NAME;
      $this->Description = SITE_DESC;
      $this->keywords = 'index, home, página inicial';
      $this->Image = RAIZ . '/imagens/logo.png';
    elseif (!empty($this->File)):
      //PÁGINAS      
      $Read->ExeRead(TB_PAGINA, "WHERE pag_name = :nm AND user_empresa = :emp AND pag_status = :st", "nm={$this->File}&emp=" . EMPRESA_CLIENTE . "&st=2");
      if ($Read->getResult()):
        $Paginas = $Read->getResult();
        $this->Schema = null;
        $this->Title = $Paginas[0]['pag_title'] . " - " . SITE_NAME;
        $this->Description = $Paginas[0]['pag_description'];
        $this->keywords = $Paginas[0]['pag_title'] . ', ' . $Paginas[0]['pag_keywords'];
        $this->Image = RAIZ . '/imagens/logo.png';
      else:
        //CATEGORIAS            
        $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND user_empresa = :emp", "nm={$this->File}&emp=" . EMPRESA_CLIENTE);
        if ($Read->getResult()):
          $Categoria = $Read->getResult();
          $this->Schema = null;
          $this->Title = $Categoria[0]['cat_title'] . " - " . SITE_NAME;
          $this->Description = $Categoria[0]['cat_description'];
          $this->keywords = $Categoria[0]['cat_title'] . ',' . $Categoria[0]['cat_keywords'];
          $this->Image = BASE . '/uploads/' . $Categoria['cat_cover'];
        else:
          //ITEM FINAL SE NÃO FOR CATEGORIA          
          if (!Check::CatByName($this->File, EMPRESA_CLIENTE)):

            //BLOG
            $Read->ExeRead(TB_BLOG, "WHERE blog_name = :nm AND blog_status = :st AND user_empresa = :emp", "nm={$this->File}&st=2&emp=" . EMPRESA_CLIENTE);
            if ($Read->getResult()):
              $blog = $Read->getResult();
              $this->Schema = "LiveBlogPosting";
              $this->Title = $blog[0]['blog_title'] . " - " . SITE_NAME;
              $this->Description = $blog[0]['blog_description'];
              $this->keywords = $blog[0]['blog_title'] . ',' . $blog[0]['blog_keywords'];
              $this->Image = BASE . '/uploads/' . $blog[0]['blog_cover'];
            endif;

            //PRODUTOS
            $Read->ExeRead(TB_PRODUTO, "WHERE prod_name = :nm AND prod_status = :st AND user_empresa = :emp", "nm={$this->File}&st=2&emp=" . EMPRESA_CLIENTE);
            if ($Read->getResult()):
              $produto = $Read->getResult();
              $this->Schema = 'product';
              $this->Title = $produto[0]['prod_title'] . " - " . SITE_NAME;
              $this->Description = $produto[0]['prod_description'];
              $this->keywords = $produto[0]['prod_title'] . ',' . $produto[0]['prod_keywords'];
              $this->Image = BASE . '/uploads/' . $produto[0]['prod_cover'];
            endif;

            //QUEM SOMOS
            $Read->ExeRead(TB_QUEMSOMOS, "WHERE quem_name = :nm AND quem_status = :st AND user_empresa = :emp", "nm={$this->File}&st=2&emp=" . EMPRESA_CLIENTE);
            if ($Read->getResult()):
              $quemsomos = $Read->getResult();
              $this->Schema = 'LiveBlogPosting';
              $this->Title = $quemsomos[0]['quem_title'] . " - " . SITE_NAME;
              $this->Description = $quemsomos[0]['quem_description'];
              $this->keywords = $quemsomos[0]['quem_title'] . ',' . $quemsomos[0]['quem_keywords'];
              $this->Image = BASE . '/uploads/' . $quemsomos[0]['quem_cover'];
            endif;

            //NOVIDADES/NOTÍCIAS
            $Read->ExeRead(TB_NOTICIA, "WHERE noti_name = :nm AND noti_status = :st AND user_empresa = :emp", "nm={$this->File}&st=2&emp=" . EMPRESA_CLIENTE);
            if ($Read->getResult()):
              $noticias = $Read->getResult();
              $this->Schema = 'LiveBlogPosting';
              $this->Title = $noticias[0]['noti_title'] . " - " . SITE_NAME;
              $this->Description = $noticias[0]['noti_description'];
              $this->keywords = $noticias[0]['noti_title'] . ',' . $noticias[0]['noti_keywords'];
              $this->Image = BASE . '/uploads/' . $noticias[0]['noti_cover'];
            endif;

            //SERVIÇOS
            $Read->ExeRead(TB_SERVICO, "WHERE serv_name = :nm AND serv_status = :st AND user_empresa = :emp", "nm={$this->File}&st=2&emp=" . EMPRESA_CLIENTE);
            if ($Read->getResult()):
              $servico = $Read->getResult();
              $this->Schema = 'Service';
              $this->Title = $servico[0]['serv_title'] . " - " . SITE_NAME;
              $this->Description = $servico[0]['serv_description'];
              $this->keywords = $servico[0]['serv_title'] . ',' . $servico[0]['serv_keywords'];
              $this->Image = BASE . '/uploads/' . $servico[0]['serv_cover'];
            endif;

            //CASES
            $Read->ExeRead(TB_CASE, "WHERE case_name = :nm AND case_status = :st AND user_empresa = :emp", "nm={$this->File}&st=2&emp=" . EMPRESA_CLIENTE);
            if ($Read->getResult()):
              $case = $Read->getResult();
              $this->Schema = 'Service';
              $this->Title = $case[0]['case_title'] . " - " . SITE_NAME;
              $this->Description = $case[0]['case_description'];
              $this->keywords = $case[0]['case_title'] . ',' . $case[0]['case_keywords'];
              $this->Image = BASE . '/uploads/' . $case[0]['case_cover'];
            endif;

          endif;
          
        endif;
      endif;
    else:
      //404
      $this->set404();
    endif;
  }

  private function set404() {
    $this->Schema = 'WebSite';
    $this->Title = "Opsss, nada encontrado! - " . SITE_NAME;
    $this->Description = SITE_DESC;
    $this->keywords = '404, Não encontrado, Ops!';
    $this->Image = RAIZ . '/imagens/logo.png';
  }

}
