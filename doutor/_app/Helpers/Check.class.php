<?php

/**
 * Check.class [ HELPER ]
 * Classe responável por manipular e validade dados do sistema!
 * 
 * @copyright (c) 2014, Robson V. Leite UPINSIDE TECNOLOGIA
 */
class Check {

  private static $Data;
  private static $Format;

  /**
   * <b>Verifica E-mail:</b> Executa validação de formato de e-mail. Se for um email válido retorna true, ou retorna false.
   * @param STRING $Email = Uma conta de e-mail
   * @return BOOL = True para um email válido, ou false
   */
  public static function Email($Email) {
    self::$Data = (string) $Email;
    self::$Format = '/[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\.\-]+\.[a-z]{2,4}$/';

    if (preg_match(self::$Format, self::$Data)):
      return true;
    else:
      return false;
    endif;
  }

  /**
   * <b>Tranforma URL:</b> Tranforma uma string no formato de URL amigável e retorna o a string convertida!
   * @param STRING $Name = Uma string qualquer
   * @return STRING = $Data = Uma URL amigável válida
   */
  public static function Name($Name) {
    self::$Format = array();
    self::$Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]|/?;:.,\\\'<>°ºª®';
    self::$Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

    self::$Data = strtr(utf8_decode($Name), utf8_decode(self::$Format['a']), self::$Format['b']);
    self::$Data = strip_tags(trim(self::$Data));
    self::$Data = str_replace(' ', '-', self::$Data);
    self::$Data = str_replace(array('-----', '----', '---', '--'), '-', self::$Data);

    return strtolower(utf8_encode(self::$Data));
  }

  /**
   * <b>Obter categoria:</b> Informe o name (url) de uma categoria para obter o ID da mesma.
   * @param STRING $category_name = URL da categoria
   * @return INT $category_id = id da categoria informada
   */
  public static function CatByName($CategoryName, $empresaId = null) {
    $empresa = (isset($empresaId) ? $empresaId : $_SESSION['userlogin']['user_empresa']);
    $read = new Read;
    $read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :name AND user_empresa = :emp", "name={$CategoryName}&emp={$empresa}");
    if ($read->getRowCount()):
      $cat = $read->getResult();
      return $cat[0]['cat_id'];
    else:
      return false;
//            echo "A categoria {$CategoryName} não foi encontrada!";
//            die;
    endif;
  }

  /**
   * <b>Obter categoria:</b> Informe o name (id) de uma categoria para obter a url da mesma.
   * @param STRING $category_name = URL da categoria
   * @return INT $category_id = id da categoria informada
   */
  public static function CatByParent($CategoryParent, $empresaId = null) {
    $empresa = (isset($empresaId) ? $empresaId : $_SESSION['userlogin']['user_empresa']);
    $categoria = null;
    $categ = null;
    $read = new Read;
    //consulta sub-categoria filha
    $read->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id AND user_empresa = :emp", "id={$CategoryParent}&emp={$empresa}");
    if ($read->getResult()):
      $sub = $read->getResult();
      //consulta sub-categoria pai
      $read->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id AND user_empresa = :emp", "id={$sub[0]['cat_parent']}&emp={$empresa}");
      if ($read->getResult()):
        $cat = $read->getResult();
        $categoria = $cat[0]['cat_name'] . '/';

        //Categoria
        $read->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id AND user_empresa = :emp", "id={$cat[0]['cat_parent']}&emp={$empresa}");
        if ($read->getResult()):
          $catsub = $read->getResult();
          $categ = $catsub[0]['cat_name'] . '/';
        endif;

      endif;
      return $categ . $categoria . $sub[0]['cat_name'] . '/';
    endif;
  }

  /**
   * <b>Obter titulo da categoria:</b> 
   * Informe a URL para obter o nome dela como Titulo  
   * @param STRING $cat_name = URL da categoria
   * @return INT $cat_title = titulo da url informada
   */
  public static function CatByUrl($CatUrl, $empresaId = null) {
    $empresa = (isset($empresaId) ? $empresaId : $_SESSION['userlogin']['user_empresa']);
    $read = new Read;
    $read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND user_empresa = :emp", "nm={$CatUrl}&emp={$empresa}");
    if ($read->getResult()):
      $cat = $read->getResult();
      return $cat[0]['cat_title'];
    endif;
  }

  /**
   * <b>Tranforma Data:</b> Transforma uma data no formato DD/MM/YY em uma data no formato TIMESTAMP!
   * @param STRING $Name = Data em (d/m/Y) ou (d/m/Y H:i:s)
   * @return STRING = $Data = Data no formato timestamp!
   */
  public static function Data($Data) {
    self::$Format = explode(' ', $Data);
    self::$Data = explode('/', self::$Format[0]);

    if (empty(self::$Format[1])):
      self::$Format[1] = date('H:i:s');
    endif;

    self::$Data = self::$Data[2] . '-' . self::$Data[1] . '-' . self::$Data[0] . ' ' . self::$Format[1];
    return self::$Data;
  }

  /**
   * <b>Limita os Palavras:</b> Limita a quantidade de palavras a serem exibidas em uma string!
   * @param STRING $String = Uma string qualquer
   * @return INT = $Limite = String limitada pelo $Limite
   */
  public static function Words($String, $Limite, $Pointer = null) {
    self::$Data = strip_tags(trim($String));
    self::$Format = (int) $Limite;

    $ArrWords = explode(' ', self::$Data);
    $NumWords = count($ArrWords);
    $NewWords = implode(' ', array_slice($ArrWords, 0, self::$Format));

    $Pointer = (empty($Pointer) ? '...' : ' ' . $Pointer );
    $Result = ( self::$Format < $NumWords ? $NewWords . $Pointer : self::$Data );
    return $Result;
  }

  /**
   * <b>Usuários Online:</b> Ao executar este HELPER, ele automaticamente deleta os usuários expirados. Logo depois
   * executa um READ para obter quantos usuários estão realmente online no momento!
   * @return INT = Qtd de usuários online
   */
  public static function UserOnline() {
    $now = date('Y-m-d H:i:s');
    $deleteUserOnline = new Delete;
    $deleteUserOnline->ExeDelete(TB_ON, "WHERE online_endview < :now", "now={$now}");

    $readUserOnline = new Read;
    $readUserOnline->ExeRead(TB_ON);
    return $readUserOnline->getRowCount();
  }

  /**
   * <b>Imagem Upload:</b> Ao executar este HELPER, ele automaticamente verifica a existencia da imagem na pasta
   * uploads. Se existir retorna a imagem redimensionada!
   * @return HTML = imagem redimencionada!
   */
  public static function Image($ImageUrl, $ImageDesc, $Classe = null, $ImageW = null, $ImageH = null) {

    self::$Data = (($ImageUrl == '../doutor/uploads/') ? 'uploads/' : $ImageUrl);

    if (file_exists(self::$Data) && !is_dir(self::$Data)):
      $patch = RAIZ;
      $imagem = self::$Data;
      return "<img src=\"{$patch}/tim.php?src={$patch}/{$imagem}&w={$ImageW}&h={$ImageH}\" alt=\"{$ImageDesc}\" title=\"{$ImageDesc}\" class=\"{$Classe}\" />";
    else:
      return "<img src=\"" . BASE . "/tim.php?src=" . BASE . "/images/default.png&w={$ImageW}&h={$ImageH}\" alt=\"{$ImageDesc}\" title=\"{$ImageDesc}\" class=\"{$Classe}\"/>";
    endif;
  }

  public static function City($Id) {
    $IdCity = (int) $Id;
    $Readcity = new Read;
    $Readcity->ExeRead(TB_CID, "WHERE cidade_id = :id", "id={$IdCity}");
    if ($Readcity->getRowCount()):
      $cidade = $Readcity->getResult();
      return $cidade[0]['cidade_nome'] . "/" . $cidade[0]['cidade_uf'];
    endif;
  }

  /**
   * <b>Grava os logs de operações do sistema no banco</b>
   * @param int $Usuário = Vide SESSION
   * @param string $Cargo = Vide SESSION
   * @param int $Nivel = Vide SESSION
   * @param string $Item = Vide Classe
   * @param string $Acao = Vide Método
   * @param timestamp $DataTime = Data tratada YYYY-MM-DD 00:00:00
   * @return boolean = Verificador da ação de gravar historico, se false não gravou, se true gravou
   */
  public static function SaveHistoric($Usuário, $Cargo, $Nivel, $Item, $Acao, $DataTime, $EmpresaId = null) {
    self::$Data = array(
      'h_user' => $Usuário,
      'h_cargo' => $Cargo,
      'h_empresa' => (!isset($EmpresaId) ? $_SESSION['userlogin']['user_empresa'] : $EmpresaId),
      'h_nivel' => $Nivel,
      'h_item' => $Item,
      'h_acao' => $Acao,
      'h_data' => $DataTime
    );
    if (in_array('', self::$Data)):
      return false;
    else:
      $Create = new Create;
      $Create->ExeCreate(TB_HISTORICO, self::$Data);
      if (!$Create->getResult()):
        return false;
      else:
        return true;
      endif;
    endif;
  }

  /**
   * 
   * @param int $UserId = ID do usuário
   * @return boolean = Caso encontre retorna o Nome e Sobrenome caso não encontre retorna false;
   */
  public static function GetUserName($UserId) {
    self::$Data = (int) $UserId;
    $Read = new Read;
    $Read->ExeRead(TB_USERS, "WHERE user_id = :id", "id=" . self::$Data);
    if (!$Read->getResult()):
      return false;
    else:
      $result = $Read->getResult();
      return $result[0]['user_name'] . " " . $result[0]['user_lastname'];
    endif;
  }

  public static function GetEmpresaName($EmpresaID) {
    self::$Data = (int) $EmpresaID;
    $Read = new Read;
    $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id=" . self::$Data);
    if (!$Read->getResult()):
      return false;
    else:
      $empresa = $Read->getResult();
      return $empresa[0]['empresa_name'];
    endif;
  }

  public static function GetEmpresaCapa($EmpresaID) {
    self::$Data = (int) $EmpresaID;
    $Read = new Read;
    $Read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id=" . self::$Data);
    if (!$Read->getResult()):
      return "images/logo-default.png";
    else:
      $empresa = $Read->getResult();
      $empresa = 'uploads/' . $empresa[0]['empresa_capa'];
      if (file_exists($empresa) && !is_dir($empresa) && !empty($empresa)):
        return $empresa;
      else:
        return "images/logo-default.png";
      endif;
    endif;
  }

  public static function GetDirectorySize($path) {
    $bytestotal = 0;
    $path = realpath('uploads/' . $path);
    if ($path !== false) {
      foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object) {
        $bytestotal += $object->getSize();
      }
    }
    $bytestotal = ($bytestotal == 0 ? 2 : $bytestotal);
    return $bytestotal;
  }

  public static function GetLastAccess() {
    $Read = new Read;
    $Read->ExeRead(TB_HISTORICO, "WHERE h_user = :user AND h_item = :it ORDER BY h_data DESC", "user={$_SESSION['userlogin']['user_id']}&it=Login");
    $result = $Read->getResult();
    if (isset($result[1])):
      return date("d/m/Y H:i", strtotime($result[1]['h_data']));
    else:
      return date("d/m/Y H:i", strtotime($result[0]['h_data']));
    endif;
  }

  /**
   * <b>Montagem do breadcrumb</b>
   * Criação automática da breadcrumb das página de SIG
   * @param array $arrBread = sessão pai     
   */
  public static function SetBreadcrumb($arrBread) {
    self::$Data = $arrBread;
    $breadLen = count(self::$Data);
    $counter = 0;
    echo '<ul id="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <i class="fa fa-home"></i> <a rel="home" href="' . RAIZ . '" title="Home" itemprop="item"><span itemprop="name">Home</span></a>
                            <meta itemprop="position" content="1" />
                    </li>';
    $i = 2;
    foreach (self::$Data as $key):
      $counter++;
      if (!empty($key)):
        if($counter != $breadLen){
        $expl = explode('/', Check::CatByParent($key['parent'], EMPRESA_CLIENTE));
        echo '<li> » </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a rel="home" href="' . RAIZ . '/' . trim(Check::CatByParent($key['parent'], EMPRESA_CLIENTE) . ($expl[0] == $key['url'] || $expl[1] == $key['url'] || $expl[2] == $key['url'] ? null : $key['url']), '/') . '"  title="' . $key['titulo'] . '" itemprop="item"><span itemprop="name">' . $key['titulo'] . '</span></a>
                            <meta itemprop="position" content="' . $i . '" />
                    </li>';
      }
      // last iteration
      else{
        echo '<li> » </li>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <span itemprop="name" class="bold">' . $key['titulo'] . '</span>
                            <meta itemprop="position" content="' . $i . '" />
                    </li>';
      }
    endif;
      $i++;
  endforeach;
    echo '</ul>';
  }

  /**
   * <b>Define o Titulo do h1 da página</b>
   * @param array $arrBread = utilizar o mesmo array do breadcrump
   * @param array $URLseo = verificador das URL para setar  titulo vindo do breadcrump
   */
  public static function SetTitulo($arrBread, $URLseo) {
    self::$Data = $arrBread;
    self::$Format = $URLseo;

    if (!empty(self::$Format[0]) && empty(self::$Format[1]) && empty(self::$Format[2])):
      echo self::$Data[0]['titulo'];
    elseif (!empty(self::$Format[0]) && !empty(self::$Format[1]) && empty(self::$Format[2])):
      echo self::$Data[1]['titulo'];
    elseif (!empty(self::$Format[0]) && !empty(self::$Format[1]) && !empty(self::$Format[2]) && empty(self::$Format[3])):
      echo self::$Data[2]['titulo'];
    else:
      echo self::$Data[3]['titulo'];
    endif;
  }

  /**
   * <b>Obter vaga:</b> Informe o name (url) de uma vaga para obter o ID da mesma.
   * @param INT $vaga_id = id da vaga
   * @return STRING $vaga_title = nome da vaga
   */
  public static function VagaById($vagaId) {
    $read = new Read;
    $read->ExeRead(TB_VAGA, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$vagaId}&emp={$_SESSION['userlogin']['user_empresa']}");
    if ($read->getResult()):
      $vaga = $read->getResult();
      return $vaga[0]['vaga_title'];
    endif;
  }

  /**
   * <b>Verifica repetições</b>
   * Metodo que verifica se existem categorias 
   * já cadastradas para evitar categorias e itens com mesmo nome nas url
   * @param string $CatTitulo = Titulo do item que está sendo criado
   * @param int $empresaId = Sessão da empresa que está tentando criar o item
   * @return boolean = Retorna true caso encontre e false caso contrário.
   */
  public static function CategoryRepeat($CatTitulo, $empresaId = null, $id = null) {
    $CatName = Check::Name($CatTitulo);
    $empresa = (isset($empresaId) ? $empresaId : $_SESSION['userlogin']['user_empresa']);
    $WHERE = (!empty($id) ? "pag_id != {$id} AND" : null);
    $read = new Read;

    //CONSULTA DE EXISTE UMA PÁGINA CRIADA COM O NOME
    $read->ExeRead(TB_PAGINA, "WHERE {$WHERE} pag_name = :nm AND user_empresa = :emp", "nm={$CatName}&emp={$empresa}");
    if ($read->getResult()):
      return true;
    else:
      //VERIFICA SE EXISTE UMA CATEGORIA CRIADA COM O MESMO NOME
      $read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND user_empresa = :emp", "nm={$CatName}&emp={$empresa}");
      if ($read->getResult()):
        return true;
      else:
        return false;
      endif;
    endif;
  }

  /**
   * <b>Pega o status do cliente</b>
   * Função para exibir o icone correto para o status atual do cliente.
   * @param int $status
   * @param int $id
   * @return string
   * return o status com uma classe montada 
   */
  public static function GetStatusCliente($status, $id) {
    self::$Data = (int) strip_tags(trim($status));
    $id = (int) strip_tags(trim($id));
    self::$Format = array(
      1 => '<button type="button" class="btn btn-success j_AlteraStatus" rel="' . $id . '" value="2"><i class="fa fa-unlock"></i></button>',
      2 => '<button type="button" class="btn btn-warning j_AlteraStatus" rel="' . $id . '" value="1"><i class="fa fa-lock"></i></button>',
      3 => '<button type="button" class="btn btn-default j_AlteraStatus" rel="' . $id . '" value="2">?</button>'
    );

    if (!empty(self::$Data)):
      return self::$Format[self::$Data];
    else:
      return '<button type="button" class="btn btn-default j_status" rel="' . $id . '" value="' . self::$Data . '"><i class="fa fa-ban"></i></button>';
    endif;
  }

  /**
   * <b>Trata url</b>
   * Função que trata a url e deixar ela limpa somente com: dominio.com.br
   * remove www. http:// etc...
   * @param string $string
   * @return string
   */
  public static function TrataUrl($string) {
    self::$Data = $string;
    self::$Data = trim(strip_tags(str_replace(array('http://', 'www.', 'https://'), '', strtolower(self::$Data))), '/');
    self::$Data = explode('/', self::$Data);
    self::$Data = self::$Data[0];
    return self::$Data;
  }

  /**
   * <b>Pega o status do item</b>
   * Função para exibir o icone correto para o status dos itens.
   * @param int $status
   * @param int $id
   * @param string $action
   * @return string
   * return o status com uma classe montada 
   */
  public static function GetSetStatus($status, $id, $action) {
    self::$Data = (int) strip_tags(trim($status));
    $id = (int) strip_tags(trim($id));
    $action = (string) strip_tags(trim($action));
    self::$Format = array(
      1 => '<button type="button" class="btn btn-success j_GlobalStatus" data-id="' . $id . '" value="2" data-action="' . $action . '"><i class="fa fa-unlock"></i></button>',
      2 => '<button type="button" class="btn btn-warning j_GlobalStatus" data-id="' . $id . '" value="1" data-action="' . $action . '"><i class="fa fa-lock"></i></button>',
      3 => '<button type="button" class="btn btn-default j_GlobalStatus" data-id="' . $id . '" value="2" data-action="' . $action . '">?</button>'
    );

    if (!empty(self::$Data)):
      return self::$Format[self::$Data];
    else:
      return '<button type="button" class="btn btn-default j_GlobalStatus" data-id="' . $id . '" value="' . self::$Data . '" data-action="' . $action . '"><i class="fa fa-ban"></i></button>';
    endif;
  }

  /**
   * <b>Pegar texto referente a palavra chave</b>
   * Este helper permite que através da id do cliente e da palavra chave, retorne o texto relacionado
   * @param type $cliente
   * @param type $palavra
   * return string
   */
  public static function GetText($cliente, $palavra) {
    self::$Format = array("cliente_id" => $cliente, "palavra_id" => $palavra);
    self::$Data = array_map('strip_tags', self::$Format);
    $Read = new Read;
    $Read->ExeRead(MPI_CONTEUDOS, "WHERE cliente_id = :cli AND palavra_id = :word", "cli=" . self::$Data['cliente_id'] . "&word=" . self::$Data['palavra_id']);
    if (!$Read->getResult()):
      return "Conteudo não encontrado";
    else:
      $read = $Read->getResult();
      if (empty($read[0]['conteudo'])):
        return "Conteudo não encontrado";
      else:
        return $read[0]['conteudo'];
      endif;

    endif;
  }

  public static function GetConstantes() {
    self::$Data = get_defined_constants(true);
    self::$Format = array();
    $restritas = array(
      'TB_FAKE',
      'TB_VAGA',
      'TB_CANDIDATO',
      'TB_CATEGORIA',
      'TB_NEWSLETTER',
      'TB_BANNER',
      'TB_QUEMSOMOS',
      'TB_ORCAMENTOS',
      'TB_PAGINA',
      'TB_EMP',
      'TB_CID',
      'TB_UF',
      'TB_USERS',
      'TB_GALLERY',
      'TB_HISTORICO',
      'TB_RELATORIOS',
      'TB_NOTAS',
      'TB_CONFIG',
      'TB_PAISES'
    );

    foreach (self::$Data['user'] as $keys => $value):
      if (strpos($keys, "TB_") !== false && !array_search($keys, $restritas)):
        $explode = explode("_", $keys);
        self::$Format[$keys] = $explode[1];
      endif;
    endforeach;

    if (count(self::$Format) == 0):
      self::$Format[0] = "Nenhuma tabela encontrada";
    endif;

    return self::$Format;
  }

  public static function SetDiretorio($string, $removeLast = null) {

    self::$Format = null;

    if (strpos($string, "/") !== false):
      self::$Data = explode("/", $string);
    else:
      self::$Data = explode(DIRECTORY_SEPARATOR, $string);
    endif;

    if (!empty($removeLast)):
      for ($i = 0; $i < $removeLast; $i++):
        array_pop(self::$Data);
      endfor;
    endif;

    foreach (self::$Data as $keys => $value):
      self::$Format .= $value . DIRECTORY_SEPARATOR;
    endforeach;

    return trim(self::$Format, DIRECTORY_SEPARATOR);
  }

  /**
   * <b>Baixa e renomeia arquivos remotos</b>
   * Verifica e separa extensão do arquivo para montar um nome novo pra ele
   * @param string $string = url remota do arquivo
   * @return string
   */
  public static function GetFileRemoto($string) {
    $fileName = "baixado-" . (substr(md5(time() + Check::Name($string)), 0, 10));

    if (strpos($string, "://") !== false):
      $extensao = substr($string, -4);
      if (strpos($extensao, ".") !== false):
        $fileName = $fileName . $extensao;
      else:
        $fileName = '.' . $extensao;
      endif;
      if (copy($string, "../../uploads/0/import/baixados/" . $fileName)):
        $fileName = "baixados" . DIRECTORY_SEPARATOR . $fileName;
      endif;
    else:
      $fileName = 'default.png';
    endif;

    return $fileName;
  }

  /**
   * <b>Tranforma Data:</b> Transforma uma data no formato DD/MM/YY em uma data no formato TIMESTAMP!
   * @param STRING $Name = Data em (d/m/Y) ou (d/m/Y H:i:s)
   * @return STRING = $Data = Data no formato timestamp!
   */
  public static function DataEUA($Data) {
    self::$Format = explode(' ', $Data);
    self::$Data = explode('/', self::$Format[0]);

    self::$Data = self::$Data[2] . '-' . self::$Data[1] . '-' . self::$Data[0];
    return self::$Data;
  }

  public static function GetIdioma($IdiomaId) {
    $IdiomaId = (int) $IdiomaId;
    $Read = new Read;
    $Read->ExeRead(TB_PAISES, "WHERE country_id = :id", "id={$IdiomaId}");
    if (!$Read->getResult()):
      return '<i class="fa fa-flag" aria-hidden="true"></i>';
    else:
      return '<i class="flag-icon flag-icon-' . $Read->getResult()[0]['country_sigla'] . '" aria-hidden="true"></i>';
    endif;
  }

  public static function GetIdiomaSigla($IdiomaId) {
    $IdiomaId = (int) $IdiomaId;
    $Read = new Read;
    $Read->ExeRead(TB_PAISES, "WHERE country_id = :id", "id={$IdiomaId}");
    if (!$Read->getResult()):
      return '<i class="fa fa-flag" aria-hidden="true"></i>';
    else:
      return '<i class="flag-icon flag-icon-' . $Read->getResult()[0]['country_sigla'] . '" aria-hidden="true"></i>';
    endif;
  }

  public static function GetIdiomaUrl($IdiomaSG) {
    $IdiomaSG = (string) $IdiomaSG;
    $sg = explode('.', $_SERVER['HTTP_HOST']);
    $url = null;

    if (strlen($sg[0]) <= 3 || $sg[0] == 'www'):
      array_shift($sg); //remove subdominio
      foreach ($sg as $key => $value):
        $url .= $value . '.';
      endforeach;
      return trim($IdiomaSG . '.' . $url, '.');
    else:
      return $_SERVER['HTTP_HOST'];
    endif;
  }

  public static function SetIdioma($idemp) {
    $id = (int) $idemp;
    $sg = explode('.', $_SERVER['HTTP_HOST']);

    if ($sg[0] != 'www' || strlen($sg[0]) == 2):
      $Read = new Read;
      $Read->FullRead("SELECT empresa_id, empresa_idioma_style FROM " . TB_EMP . " WHERE empresa_idioma_sg = :sg AND (empresa_id = :emp OR empresa_parent = :emp)", "sg={$sg[0]}&emp={$id}");
      if (!$Read->getResult()):
        return $id;
      else:
        return $Read->getResult()[0]['empresa_id'];
      endif;
    else:
      return $id;
    endif;
  }

  public static function SetIdiomaStyle($idemp) {
    $id = (int) $idemp;
    $sg = explode('.', $_SERVER['HTTP_HOST']);

    if ($sg[0] != 'www' || strlen($sg[0]) == 2):
      $Read = new Read;
      $Read->FullRead("SELECT empresa_idioma_style FROM " . TB_EMP . " WHERE empresa_idioma_sg = :sg AND (empresa_id = :emp OR empresa_parent = :emp)", "sg={$sg[0]}&emp={$id}");
      if ($Read->getResult()):
        return $Read->getResult()[0]['empresa_idioma_style'];
      endif;
    endif;
  }

  /**
   * <b>Monta a URL com base em um vetor</b>
   * @param array $arrayURL = Entrada com indice de urls
   * @return boolean|string Se não for um array com pelo menos uma entrada retorna falso
   */
  public static function AmmountURL(array $arrayURL) {
    $url = null;
    if (count($arrayURL) >= 1):
      foreach ($arrayURL as $key => $values):
        $url .= "/" . $values;
      endforeach;
      return $url;
    else:
      return false;
    endif;
  }

  public static function GetLastCategory(array $arrayURL) {
    if (count($arrayURL) <= 1):
      return false;
    else:
      unset($arrayURL[count($arrayURL) - 1]);
      return array_pop($arrayURL);
    endif;
  }

  /**
   * <b>Modulo includes</b>
   * Função que faz a chamada dos módulos a serem incluídos na home do site
   * @param type $string = numeros dos modulos de include
   */
  public static function SetAtributosInc($string){
    
  }
}
