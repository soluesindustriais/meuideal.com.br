<?
$h1         = 'Meu Ideal';
$title      = 'Início - Meu Ideal';
$desc       = 'Meu Ideal - Conheça tudo sobre Marketing Multinível';  
$var        = 'Home';
include('inc/head.php');
?>
</head>
<body>
<? include('inc/menu-topo.php'); ?>
<div class="home-slider">
<? include('inc/caroussel-home.php'); ?>
</div>
<main class="main-home">
<div class="content home-destaques">
		<section class="index-section col-12">
			<div class="container">
				<div class="justify-content-center">
					<h2 class="text-black text-center">Meu Ideal</h2>
					<h3 class="text-center">DESTAQUES</h3>
				</div>
			</div>
		</section>
<? include('inc/caroussel-home-destaques.php'); ?>
</div>
<div class="content home-videos">
		<section class="index-section col-12">
			<div class="container">
				<div class="justify-content-center">
					<h2 class="text-black text-center">Meu Ideal</h2>
					<h3 class="text-center">VIDEOS</h3>
				</div>
			</div>
		</section>
<? include('inc/caroussel-home-videos.php'); ?>
</div>
<div class="content home-cast">
		<section class="index-section col-12">
			<div class="container">
				<div class="justify-content-center">
					<h2 class="text-black text-center">Meu Ideal</h2>
					<h3 class="text-center">CAST</h3>
				</div>
			</div>
		</section>
<? include('inc/caroussel-home-cast.php'); ?>
</div>
<div class="home-form">
		<div class="container">
			<div class="justify-content-center">
				<h2 class="text-primary text-center">Faça parte da plataforma que vai fazer você ter a sua melhor versão!</h2>
			</div>
		</div>
	<? include('inc/home-form.php'); ?>
</div>
<div class="content home-blog">
		<section class="index-section col-12">
			<div class="container">
				<div class="justify-content-center">
					<h2 class="text-black text-center">Meu Ideal</h2>
					<h3 class="text-center">BLOG</h3>
				</div>
			</div>
		</section>
<? include('inc/home-gallery.php'); ?>
</div>
<? include('inc/footer.php'); ?>
</body>
</html>