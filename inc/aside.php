<?php if (count($URL) > 1): ?> 
  <aside class="main-aside"> 
    <div class="fb-page" data-href="http://www.facebook.com/<?= $paginaFacebook ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="http://www.facebook.com/<?= $paginaFacebook ?>"><a href="http://www.facebook.com/<?= $paginaFacebook ?>"><?= $nomeSite ?></a></blockquote></div></div>

    <?php include 'inc/sub-menu-aside.php'; ?>
    
    <br class="clear"/>
    <div class="clear"></div>
    <hr>
      
    <div class="callPhone">
      <h3>Contatos</h3>
      <ul>
        <li><i class="fa fa-phone"></i> <?php echo $ddd; ?> <strong><?php echo $fone; ?></strong></li>
        <!--<li><i class="fa fa-phone"></i> <strong><?php echo $fone2; ?></strong></li>-->
        <!--<li><i class="fa fa-whatsapp"></i> <?php echo $ddd; ?> <strong><?php echo $fone3; ?></strong></li>-->        
      </ul>
      
      <div class="clear"></div>
      <a href="<?= RAIZ; ?>/contato" title="Entre em contato" class="btn_orc j_event" data-category="<?= strtoupper(Check::GetLastCategory($URL)); ?>" data-event="Clicou entrar em contato" data-label="<?php Check::SetTitulo($arrBreadcrump, $URL); ?>" style="width: 100%;"><i class="fa fa-envelope"></i> Entre em contato</a>
    </div>
  </aside>
<?php endif; ?>
