<nav class="menu-topo navbar navbar-light justify-content-between">
  <div class="logo"><a rel="nofollow" href="<?=$url?>" title="Voltar a página inicial"><img src="<?=$url?>images/img-home/logo.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>"></a></div>
  <form class="form-inline">
  	 <div>
      <button class="btn-entrar btn" type="button">ENTRAR</button>
    </div>
    <div>
      <button class="btn-cadastra btn" type="button">CADASTRA-SE</button>
    </div>
    <div class="btn-icones"> <a href="<?=$url?>" title="Pesquisar"><i class="fas fa-search"></i></a></div>
     <div class="btn-icones"> <a href="<?=$url?>" title="Pesquisar"><i class="fas fa-align-justify"></i></a></div>
      </form>

<!--     <div class="mmenu-trigger">
		<button class="hamburger hamburger--collapse" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
	</div> -->
</nav>