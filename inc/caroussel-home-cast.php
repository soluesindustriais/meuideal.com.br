<div class="home-caroussel-4">
<div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner" style="min-height: 195px">
    <div class="carousel-item active">
      <img class="col-md-3 mx-2 thumb-cast border border-secondary" src="<?=$url?>images/img-home/cast-01.jpg" alt="Primeiro slide">
      <img class="col-md-3 mx-2 thumb-cast border border-secondary" src="<?=$url?>images/img-home/cast-02.jpg" alt="Segundo slide">
      <img class="col-md-3 mx-2 thumb-cast border border-secondary" src="<?=$url?>images/img-home/cast-03.jpg" alt="Terceiro slide">
      <img class="col-md-3 mx-2 thumb-cast border border-secondary" src="<?=$url?>images/img-home/cast-04.jpg" alt="Quarto slide">
    </div>
    <div class="carousel-item ">
      <img class="col-md-3 mx-2 thumb-cast border border-secondary" src="<?=$url?>images/img-home/cast-05.jpg" alt="Quinto slide">
      <img class="col-md-3 mx-2 thumb-cast border border-secondary" src="<?=$url?>images/img-home/cast-06.jpg" alt="Sexto slide">
      <img class="col-md-3 mx-2 thumb-cast border border-secondary" src="<?=$url?>images/img-home/cast-07.jpg" alt="Sétimo slide">
      <img class="col-md-3 mx-2 thumb-cast border border-secondary" src="<?=$url?>images/img-home/cast-08.jpg" alt="Oitavo slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  <div class="col-lg-12 mt-5">
    <button class="btn btn-primary center-block" type="button">VER MAIS</button>
  </div>
</div>