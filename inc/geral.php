<?
$nomeSite			= 'Meu Ideal';
// $slogan				= 'Marketing Digital';
define('WEBROOT', 'projetos/meuideal.com.br/'); 
$url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/' . WEBROOT;
$ddd				= '';
$fone				= '+55-11-94749-4920';
$fone2				= '+1 (407) 360-8873';
// $fone3				= '2123-4444';
$emailContato		= 'info@meuideal.com.br';
$rua				= 'CROWN HAVE DR';
$bairro				= 'COMPASS BAY';
$cidade				= 'KISSIMMEE';
$UF					= 'FLORIDA';
$cep				= 'ZIP CODE 34746';
$latitude			= '-28.336896';
$longitude			= '-81.490350';
$idAnalytics		= 'ID do Google Analytics';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br

// $getURL = trim(strip_tags(filter_input(INPUT_GEmeuidealT, 'url', FILTER_DEFAULT)));
// $urlPagina = explode("/", $getURL);
// $ContPag = sizeof($urlPagina);
// $urlPagina = $urlPagina[0];
// $urlPagina == "index" ? $urlPagina = "" : "";

//reCaptcha do Google
$siteKey = '6LdjwnAUAAAAADJZZbBdPI37bSCIcoAq1oXv8c-y';
$secretKey = '6LdjwnAUAAAAAJ9Ro5w3nPUDLxg8OkiJhS9kM6Uj';
//Breadcrumbs
$caminho 			= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
</div>
';
$caminho2	= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
		<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
';
$caminho3	= '
<div class="title-breadcrumb breadmpi">
	<div class="wrapper">
		<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
			<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
			<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"> <a href="'.$url.'informacoes">Informações</a> </span> »
				<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
				</div>
			</div>
		</div>
		<h1>'.$h1.'</h1>
	</div>
</div>
';
//Pasta de imagens, Galeria, url Facebook, etc.
$pasta 				= 'imagens/informacoes/';
//Redes sociais
$idFacebook			= 'Colocar o ID da página do Facebook'; //Link para achar o ID da página do Facebook http://graph.facebook.com/Nome da página do Facebook
$idGooglePlus		= 'http://plus.google.com.br'; // ID da página da empresa no Google Plus
// $paginaFacebook		= 'PAGINA DO FACEBOOK DO CLIENTE';
$author = ''; // Link do perfil da empresa no g+ ou deixar em branco
//Reescrita dos links dos telefones
$link_tel = str_replace('(', '', $ddd);
$link_tel = str_replace(')', '', $link_tel);
$link_tel = str_replace('11', '', $link_tel);
$link_tel .= '5511'.$fone;
$link_tel = str_replace('-', '', $link_tel);
$creditos			= 'Doutores da Web - Marketing Digital';
$siteCreditos		= 'www.doutoresdaweb.com.br';
$caminhoBread 			= '
<div class="title-breadcrumb">
	<div class="wrapper">
		<h1 class="block-title">'.$h1.'</h1><br>
		<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
			<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
';
$caminhoBread2	= '
<div class="title-breadcrumb">
	<div class="wrapper">
		<h1 class="block-title">'.$h1.'</h1>
		<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
			<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
			<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
				<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
				</div>
			</div>
		</div>
	</div>
</div>
';
?>