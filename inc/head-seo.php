<?php
$getURL = trim(strip_tags(filter_input(INPUT_GET, 'url', FILTER_DEFAULT)));
$setURL = (empty($getURL) ? 'index' : $getURL);
$URL = explode('/', $setURL);
$SEO = new Seo($setURL);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br" itemscope itemtype="https://schema.org/<?= $SEO->getSchema(); ?>"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <script src="<?= RAIZ; ?>/js/jquery.js"></script>
    <script async src="<?= RAIZ; ?>/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="<?= RAIZ; ?>/css/normalize.css">
    <link rel="stylesheet" href="<?= RAIZ; ?>/css/style.css">
    <link rel="stylesheet" href="<?= RAIZ; ?>/css/doutor.css">
    <!--<link rel="stylesheet" href="<?= RAIZ; ?>/css/simple-grid.css">-->
    <link rel="stylesheet" href="<?= RAIZ; ?>/css/font-awesome.css">
    <!-- <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'> -->
    <!-- BOTAO SCROLL -->
    <script async src="<?= RAIZ; ?>/js/jquery.scrollUp.min.js"></script>
    <script async src="<?= RAIZ; ?>/js/scroll.js"></script>
    <!-- /BOTAO SCROLL -->
    
    <!-- MENU  MOBILE -->
    <script async src="<?= RAIZ; ?>/js/jquery.slicknav.js"></script>
    <!-- /MENU  MOBILE -->
    <title><?= $SEO->getTitle(); ?></title>
    <base href="<?= RAIZ; ?>">
    <meta name="description" content="<?= $SEO->getDescription(); ?>">
    <meta name="keywords" content="<?= $SEO->getkeywords(); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="geo.position" content="<?= $latitude . ";" . $longitude ?>">
    <meta name="geo.placename" content="<?= $cidade . "-" . $UF ?>">
    <meta name="geo.region" content="<?= $UF ?>-BR">
    <meta name="ICBM" content="<?= $latitude . ";" . $longitude ?>">
    <meta name="robots" content="index,follow">
    <meta name="rating" content="General">
    <meta name="revisit-after" content="7 days">
    <link rel="canonical" href="<?= RAIZ; ?>/<?= $getURL; ?>">
    <?php
    if (empty($author)):
    echo '<meta name="author" content="' . $nomeSite . '">';
    else:
    echo '<link rel="author" href="' . $author . '">';
    endif;
    ?>
    <link rel="shortcut icon" href="<?= RAIZ; ?>/imagens/favicon.png">
    <meta property="og:region" content="Brasil">
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?= $SEO->getTitle(); ?>" />
    <meta property="og:description" content="<?= $SEO->getDescription(); ?>" />
    <meta property="og:image" content="<?= $SEO->getImage(); ?>" />
    <meta property="og:url" content="<?= RAIZ; ?>/<?= $getURL; ?>" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:site_name" content="<?= $nomeSite ?>">
    <?php
    if ($idFacebook <> '')
    {
    echo '<meta property="fb:admins" content="'.$idFacebook.'">';
    }
    ?>