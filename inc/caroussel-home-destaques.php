<div class="home-caroussel-2">
<div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators-2" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner" style="min-height: 500px">
    <div class="carousel-item home-group-cursos active">
      <img class="col-md-6 home-cursos" src="<?=$url?>images/img-home/destaques-01.jpg" alt="Primeiro slide">
      <img class="col-md-6 home-cursos" src="<?=$url?>images/img-home/destaques-02.jpg" alt="Segundo slide">
    </div>
    <div class="carousel-item home-group-cursos">
      <img class="col-md-6 home-cursos" src="<?=$url?>images/img-home/cursos/cursos-03.jpg" alt="Terceiro slide">
      <img class="col-md-6 home-cursos" src="<?=$url?>images/img-home/cursos/cursos-04.jpg" alt="Quarto slide">
    </div>
    <div class="carousel-item home-group-cursos">
      <img class="col-md-6 home-cursos" src="<?=$url?>images/img-home/cursos/cursos-05.jpg" alt="Quinto slide">
      <img class="col-md-6 home-cursos" src="<?=$url?>images/img-home/cursos/cursos-06.jpg" alt="Sexto slide">
    </div>
    <div class="carousel-item home-group-cursos">
      <img class="col-md-6 home-cursos" src="<?=$url?>images/img-home/cursos/cursos-07.jpg" alt="Sétimo slide">
      <img class="col-md-6 home-cursos" src="<?=$url?>images/img-home/cursos/cursos-08.jpg" alt="Oitavo slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>