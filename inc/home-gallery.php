<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--####
### How to add in your boostrap project
1) Add jQuery "<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>"
2) Download fancybox (https://github.com/fancyapps/fancyBox)
3) Or use CDN (http://cdnjs.com/libraries/fancybox)
####--!>

<!-- References: https://github.com/fancyapps/fancyBox -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

<div class="container">
  <div class="row">
    <div class='gallery'>
            <div class='col-sm-4 col-xs-6 col-md-4 col-lg-4'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-center'>
                        <h3 class="texto-galeria">Saiba sobre Marketing</h3>
                        <p class="paragrafo-galeria">O marketing bla bla bla é possivelmente bla bla bla bla, com muito bla e também bla, assim podemos considerar que bla é igual ou maior que bla. Resumindo, bla bla bla é bla maior.
                    </div> <!-- text-center / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-4 col-lg-4'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-center'>
                        <h3 class="texto-galeria">Saiba sobre Vendas</h3>
                        <p class="paragrafo-galeria">As vendas bla bla bla é possivelmente bla bla bla bla, com muito bla e também bla, assim podemos considerar que bla é igual ou maior que bla. Resumindo, bla bla bla é bla maior.
                    </div> <!-- text-center / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-4 col-lg-4'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-center'>
                        <h3 class="texto-galeria">Saiba sobre Funil</h3>
                        <p class="paragrafo-galeria">O funil bla bla bla é possivelmente bla bla bla bla, com muito bla e também bla, assim podemos considerar que bla é igual ou maior que bla. Resumindo, bla bla bla é bla maior.
                    </div> <!-- text-center / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-4 col-lg-4'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-center'>
                        <h3 class="texto-galeria">Saiba sobre E-commerce</h3>
                        <p class="paragrafo-galeria">O e-commerce bla bla bla é possivelmente bla bla bla bla, com muito bla e também bla, assim podemos considerar que bla é igual ou maior que bla. Resumindo, bla bla bla é bla maior.
                    </div> <!-- text-center / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-4 col-lg-4'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-center'>
                        <h3 class="texto-galeria">Saiba sobre SEO</h3>
                        <p class="paragrafo-galeria">O SEO bla bla bla é possivelmente bla bla bla bla, com muito bla e também bla, assim podemos considerar que bla é igual ou maior que bla. Resumindo, bla bla bla é bla maior.
                    </div> <!-- text-center / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-4 col-lg-4'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-center'>
                        <h3 class="texto-galeria">Saiba sobre Multinível</h3>
                        <p class="paragrafo-galeria">O muiltinível bla bla bla é possivelmente bla bla bla bla, com muito bla e também bla, assim podemos considerar que bla é igual ou maior que bla. Resumindo, bla bla bla é bla maior.
                    </div> <!-- text-center / end -->
                </a>
            </div> <!-- col-6 / end -->
        </div> <!-- list-group / end -->
    <div class="col-lg-12 mb-5">
      <button class="btn btn-primary center-block" type="button">VER MAIS</button>
    </div>
  </div> <!-- row / end -->
</div> <!-- container / end -->


<script>
    $(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
});
</script>