<div class="clearfix"></div>
<footer>
  <div class="bg-footer pb-4">
    <div class="container-fluid  padding-90">

    <div class="row">
      <div class="col-12">
        <div class="row">  
          <img class="logo-footer" src="images/img-home/logo-footer.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>">
        </div>
          <hr class="hr-footer">
      </div> 
    </div>
      <div class="col-12 nopadding" >
    <div class="container-fluid nopadding">
      <div class="row">
        <div class="col-md-6 col-sm-6"> 
          <p><i class="fas fa-map-marker-alt"></i> <?= $rua . " - " . $bairro . " <br> " . $cidade . "-" . $UF . " - " . $cep ?></p>
        </div>
        <div class="col-md-6 col-sm-6" >     
          <div class="pull-right">               
          <a rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
          <a rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>" target="_blank" title="CSS W3C" ><i class="fab fa-css3"></i> <strong>W3C</strong></a>
          </div>
        </div>
      </div>
    </div>
      </div>
    </div>
  </div>
</footer>

<script>
    jQuery(document).ready(function($) {
 
        $('#myCarousel').carousel({
                interval: 5000
        });
 
        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click(function () {
        var id_selector = $(this).attr("id");
        try {
            var id = /-(\d+)$/.exec(id_selector)[1];
            console.log(id_selector, id);
            jQuery('#myCarousel').carousel(parseInt(id));
        } catch (e) {
            console.log('Regex failed!', e);
        }
    });
        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
                 var id = $('.item.active').data('slide-number');
                $('#carousel-text').html($('#slide-content-'+id).html());
        });
});
</script>

<script defer src="https://use.fontawesome.com/releases/v5.5.0/js/all.js" integrity="sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0" crossorigin="anonymous"></script>
</footer>