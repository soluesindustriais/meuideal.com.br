<div class="row home-videos-grid">
  <div class="col-md-6 home-main-video">
      <video id="mainVideo" width="400" controls>
        <source id="myVideoHome" src="" type="video/mp4">
      </video>
  </div>

  <div class="col-md-6 home-other-video">
    <ul id="navVideo" class="hide-bullets">
      <li class="home-videos-thumbs col-md-6"><img class="videoSet thumbnail" src="http://placehold.it/150x150&text=teste%20abcdfger" data-video="https://www.w3schools.com/html/mov_bbb.mp4"></li>
      <li class="home-videos-thumbs col-md-6"><img class="videoSet thumbnail" src="http://placehold.it/150x150&text=teste%20abcdfger" data-video="https://www.w3schools.com/tags/movie.mp4"></li>
      <li class="home-videos-thumbs col-md-6"><img class="videoSet thumbnail" src="http://placehold.it/150x150&text=teste%20abcdfger" data-video="https://www.w3schools.com/howto/rain.mp4"></li>
      <li class="home-videos-thumbs col-md-6"><img class="videoSet thumbnail" src="http://placehold.it/150x150&text=teste%20abcdfger" data-video="https://www.w3schools.com/tags/movie.mp4"></li>
    </ul>
  </div>
    <div class="col-lg-12">
      <button class="btn btn-primary center-block" type="button">VER MAIS</button>
    </div>
</div>


<script>

  $( document ).ready(function() {
    var firstVideo = $('#navVideo .videoSet:first-child').attr('data-video');
    $('#myVideoHome').attr('src', firstVideo);
    $('#mainVideo').get(0).load();
  });

    $('#navVideo .videoSet').on('click', function(){
      var videoSrc = $(this).attr('data-video');
      console.log(videoSrc);
      $('#myVideoHome').attr('src', videoSrc);
      $('#mainVideo').get(0).load();

    });
  
</script>