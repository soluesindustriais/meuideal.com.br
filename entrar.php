<?
$h1         = 'Entrar';

<? include('inc/menu-topo.php'); ?>
<div class="cadastro-bg">
<div class="cadastro-logo">
    <a rel="nofollow" href="<?=$url?>" title="Voltar a página inicial"><img src="<?=$url?>images/img-home/logo-footer.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>"></a>
</div>
<div class="container cadastro-form">
<div class="col-md-5">
    <div class="form-area">  
        <form role="form">
            <br>
            <h2 style="margin-bottom: 25px;">Insira os seus dados:</h2>
            <div class="form-group">
                <input type="text" class="form-control" id="email" name="email" placeholder="Email" required>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="senha" name="senha" placeholder="Senha" required>
            </div>
            <label><input type="checkbox" name="terms"><a href="#">Esqueci minha senha</a>.</label>
            <br>
            <button type="button" id="submit" name="submit" class="btn btn-main pull-right">ENTRAR</button>
            <button type="button" id="submit" name="submit" class="btn btn-second pull-right">Cadastrar</button>
        </form>
    </div>
</div>
</div>
</div>

<? include('inc/footer.php'); ?>
</body>
</html>

<script>
    $(document).ready(function(){ 
    $('#characterLeft').text('140 characters left');
    $('#message').keydown(function () {
        var max = 140;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('You have reached the limit');
            $('#characterLeft').addClass('red');
            $('#btnSubmit').addClass('disabled');            
        } 
        else {
            var ch = max - len;
            $('#characterLeft').text(ch + ' characters left');
            $('#btnSubmit').removeClass('disabled');
            $('#characterLeft').removeClass('red');            
        }
    });    
});
</script>